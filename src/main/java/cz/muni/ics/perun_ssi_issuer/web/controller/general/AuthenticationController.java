package cz.muni.ics.perun_ssi_issuer.web.controller.general;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGIN;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGIN_ERROR;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGOUT;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGOUT_COMPLETE;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_LOGIN;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_LOGIN_ERROR;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_LOGOUT;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_LOGOUT_COMPLETE;

@Controller
@Slf4j
public class AuthenticationController extends AppController {

    public static final String MODEL_ATTR_LOGIN_BUTTON_LINK = "loginButtonLink";

    // === CONSTRUCTORS === //

    @Autowired
    public AuthenticationController(@NonNull ApplicationProperties applicationProperties,
                                    @NonNull OidcProperties oidcProperties) {
        super(applicationProperties, oidcProperties);
    }

    @RequestMapping(PATH_LOGIN)
    public String login(Model model) {
        model.addAttribute(MODEL_ATTR_LOGIN_BUTTON_LINK, constructLoginLink());
        return VIEW_LOGIN;
    }

    @RequestMapping(PATH_LOGIN_ERROR)
    public String loginError() {
        return VIEW_LOGIN_ERROR;
    }

    @GetMapping(PATH_LOGOUT)
    public String logout() {
        return VIEW_LOGOUT;
    }

    @RequestMapping(PATH_LOGOUT_COMPLETE)
    public String logoutComplete() {
        return VIEW_LOGOUT_COMPLETE;
    }

}
