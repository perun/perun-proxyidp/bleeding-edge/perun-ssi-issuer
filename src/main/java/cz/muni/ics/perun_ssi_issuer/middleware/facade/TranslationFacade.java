package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.Translations;
import lombok.NonNull;

import javax.transaction.Transactional;

public interface TranslationFacade {

    String SCH_ATTR_DEF_I18N_PREFIX = "sch_attr_def_";

    String SCH_DEF_I18N_PREFIX = "sch_def_";

    String PRES_SCH_ATTR_DEF_I18N_PREFIX = "pres_sch_attr_def_";

    String PRES_SCH_DEF_I18N_PREFIX = "pres_sch_def_";

    static String getNameTranslationKey(@NonNull String key) {
        return key + "_name";
    }

    static String getDescTranslationKey(@NonNull String key) {
        return key + "_desc";
    }

    static String getSchAttrDefI18nDefBase(@NonNull String definitionName) {
        return SCH_ATTR_DEF_I18N_PREFIX + definitionName;
    }

    static String getSchDefI18nDefBase(@NonNull String definitionName) {
        return SCH_DEF_I18N_PREFIX + definitionName;
    }

    static String getPresSchAttrDefI18nDefBase(@NonNull String definitionName) {
        return PRES_SCH_ATTR_DEF_I18N_PREFIX + definitionName;
    }

    static String getPresSchDefI18nDefBase(@NonNull String definitionName) {
        return PRES_SCH_DEF_I18N_PREFIX + definitionName;
    }

    static String getSchAttrDefI18nNameKey(@NonNull String definitionName) {
        return getNameTranslationKey(getSchAttrDefI18nDefBase(definitionName));
    }

    static String getSchAttrDefI18nDescKey(@NonNull String definitionName) {
        return getDescTranslationKey(getSchAttrDefI18nDefBase(definitionName));
    }

    static String getSchDefI18nNameKey(@NonNull String definitionName) {
        return getNameTranslationKey(getSchDefI18nDefBase(definitionName));
    }

    static String getSchDefI18nDescKey(@NonNull String definitionName) {
        return getDescTranslationKey(getSchDefI18nDefBase(definitionName));
    }

    static String getPresSchAttrDefI18nNameKey(@NonNull String definitionName) {
        return getNameTranslationKey(getPresSchAttrDefI18nDefBase(definitionName));
    }

    static String getPresSchAttrDefI18nDescKey(@NonNull String definitionName) {
        return getDescTranslationKey(getPresSchAttrDefI18nDefBase(definitionName));
    }

    static String getPresSchDefI18nNameKey(@NonNull String definitionName) {
        return getNameTranslationKey(getPresSchDefI18nDefBase(definitionName));
    }

    static String getPresSchDefI18nDescKey(@NonNull String definitionName) {
        return getDescTranslationKey(getPresSchDefI18nDefBase(definitionName));
    }

    @Transactional
    void createSchemaAttributeDefinitionTranslations(@NonNull Translations translations, @NonNull String definitionName);

    @Transactional
    void updateSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                     @NonNull String definitionName);

    @Transactional
    void updateSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                     @NonNull String definitionName,
                                                     @NonNull String oldName);

    @Transactional
    void deleteSchemaAttributeDefinitionTranslations(@NonNull String definitionName);

    Translations getSchemaAttributeDefinitionTranslations(@NonNull String definitionName);

    @Transactional
    void createSchemaDefinitionTranslations(@NonNull Translations translations, @NonNull String definitionName);

    @Transactional
    void updateSchemaDefinitionTranslations(@NonNull Translations translations,
                                            @NonNull String definitionName);

    @Transactional
    void updateSchemaDefinitionTranslations(@NonNull Translations translations,
                                            @NonNull String definitionName,
                                            @NonNull String oldName);

    @Transactional
    void deleteSchemaDefinitionTranslations(@NonNull String definitionName);

    Translations getSchemaDefinitionTranslations(@NonNull String definitionName);

    @Transactional
    void createPresentationSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                                 @NonNull String definitionName);

    @Transactional
    void updatePresentationSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                                 @NonNull String definitionName);

    @Transactional
    void updatePresentationSchemaAttributeDefinitionTranslations(@NonNull Translations translations,
                                                                 @NonNull String definitionName,
                                                                 @NonNull String oldName);

    @Transactional
    void deletePresentationSchemaAttributeDefinitionTranslations(@NonNull String definitionName);

    Translations getPresentationSchemaAttributeDefinitionTranslations(@NonNull String definitionName);

    @Transactional
    void createPresentationSchemaDefinitionTranslations(@NonNull Translations translations, @NonNull String definitionName);

    @Transactional
    void updatePresentationSchemaDefinitionTranslations(@NonNull Translations translations,
                                            @NonNull String definitionName);

    @Transactional
    void updatePresentationSchemaDefinitionTranslations(@NonNull Translations translations,
                                            @NonNull String definitionName,
                                            @NonNull String oldName);

    @Transactional
    void deletePresentationSchemaDefinitionTranslations(@NonNull String definitionName);

    Translations getPresentationSchemaDefinitionTranslations(@NonNull String definitionName);

}
