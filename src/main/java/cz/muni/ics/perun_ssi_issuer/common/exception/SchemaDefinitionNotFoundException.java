package cz.muni.ics.perun_ssi_issuer.common.exception;

public class SchemaDefinitionNotFoundException extends Exception {

    public SchemaDefinitionNotFoundException() {
        super();
    }

    public SchemaDefinitionNotFoundException(String message) {
        super(message);
    }

    public SchemaDefinitionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public SchemaDefinitionNotFoundException(Throwable cause) {
        super(cause);
    }

    protected SchemaDefinitionNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
