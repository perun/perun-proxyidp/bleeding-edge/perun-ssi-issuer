package cz.muni.ics.perun_ssi_issuer.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = LocalizationMapValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface LocalizationMapConstraint {

    String message() default
            "{cz.muni.ics.perun_ssi_issuer.common.validation.LocalizationMapConstraint.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
