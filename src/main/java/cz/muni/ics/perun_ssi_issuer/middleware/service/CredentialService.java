package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialConnectionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialCredentialDefinitionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDTO;
import lombok.NonNull;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface CredentialService {

    CredentialDTO issueCredential(@NonNull Long connectionId,
                                  @NonNull Long credentialDefinitionId,
                                  @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            UserNotFoundException, NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, PerunUserNotFoundException;

    CredentialDTO getCredential(@NonNull Long connectionId,
                                @NonNull Long credentialDefinitionId,
                                @NonNull Long credentialId,
                                @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, CredentialCredentialDefinitionMismatchException,
            CredentialConnectionMismatchException, CredentialNotFoundException;

    CredentialDTO getCredentialForDetail(@NonNull Long connectionId,
                                         @NonNull Long credentialId,
                                         @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, ConnectionNotFoundException,
            DatabaseInconsistencyException, UnauthorizedConnectionAccessException,
            CredentialConnectionMismatchException, CredentialNotFoundException;

    List<CredentialDTO> getCredentialsByDefinitionId(@NonNull Long credentialDefinition) throws CredentialDefinitionNotFoundException;

    CredentialDTO getCredentialCheckStatus(@NonNull Long connectionId,
                                           @NonNull Long credentialDefinitionId,
                                           @NonNull Long credentialId,
                                           @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, CredentialCredentialDefinitionMismatchException,
            CredentialConnectionMismatchException, CredentialNotFoundException;

}
