package cz.muni.ics.perun_ssi_issuer.web.controller.admin;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentationSchemaAttributeDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentationSchemaDefinitionService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaDefinitionForm;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_DEFINITIONS_CREATE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_DEFINITIONS_DELETE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_DEFINITIONS_EDIT;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PRES_SCHEMA_DEF_ID;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRES_SCHEMA_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRES_SCHEMA_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRES_SCHEMA_DEFINITIONS_FORM;

@Controller
@Slf4j
public class PresentationSchemaDefinitionController extends AppController {

    public static final String MODEL_ATTR_SCHEMA_DEF_FORM = "form";

    public static final String MODEL_ATTR_PRES_SCHEMA_DEF_ID = "schemaDefId";

    public static final String MODEL_ATTR_SCHEMA_DEFINITIONS = "definitions";

    public static final String MODEL_ATTR_SCHEMA_DEFINITION = "definition";

    public static final String MODEL_ATTR_DELETE_FAILED = "deleteFailed";

    public static final String MODEL_ATTR_SCHEMA_ATTR_DEFINITIONS = "attrDefinitions";

    private final PresentationSchemaDefinitionService service;

    private final PresentationSchemaAttributeDefinitionService attributeDefinitionService;

    @Autowired
    public PresentationSchemaDefinitionController(
            @NonNull ApplicationProperties applicationProperties,
            @NonNull OidcProperties oidcProperties,
            PresentationSchemaDefinitionService presentationSchemaDefinitionService,
            PresentationSchemaAttributeDefinitionService attributeDefinitionService)
    {
        super(applicationProperties, oidcProperties);
        this.service = presentationSchemaDefinitionService;
        this.attributeDefinitionService = attributeDefinitionService;
    }


    @GetMapping(PATH_PRES_SCHEMA_DEFINITIONS)
    public String schemaDefinitions(Model model) {
        List<PresentationSchemaDefinitionDTO> schemaDefinitions = service.getDefinitions();
        model.addAttribute(MODEL_ATTR_SCHEMA_DEFINITIONS, schemaDefinitions);
        return VIEW_PRES_SCHEMA_DEFINITIONS;
    }

    @GetMapping(PATH_PRES_SCHEMA_DEFINITIONS_DETAIL)
    public String schemaDefinition(@PathVariable(PRES_SCHEMA_DEF_ID) Long id, Model model)
            throws PresentationSchemaDefinitionNotFoundException
    {
        PresentationSchemaDefinitionDTO schemaDefinition = service.getDefinition(id);
        model.addAttribute(MODEL_ATTR_SCHEMA_DEFINITION, schemaDefinition);
        return VIEW_PRES_SCHEMA_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_PRES_SCHEMA_DEFINITIONS_CREATE)
    public String createDefinition(@ModelAttribute(MODEL_ATTR_SCHEMA_DEF_FORM)
                                       PresentationSchemaDefinitionForm form,
                                   Model model) {
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_DEF_ID, null);
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEFINITIONS, attributeDefinitionService.getDefinitions());
        return VIEW_PRES_SCHEMA_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_PRES_SCHEMA_DEFINITIONS_CREATE)
    public String createDefinitionSubmit(@Valid @ModelAttribute(MODEL_ATTR_SCHEMA_DEF_FORM)
                                         PresentationSchemaDefinitionForm form,
                                         BindingResult bindingResult,
                                         Model model) {
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEFINITIONS, attributeDefinitionService.getDefinitions());
        if (bindingResult.hasErrors()) {
            return VIEW_PRES_SCHEMA_DEFINITIONS_FORM;
        }
        PresentationSchemaDefinitionDTO schemaDefinition = service.createDefinition(form);
        model.addAttribute(PRES_SCHEMA_DEF_ID, schemaDefinition.getId());
        return "redirect:" + PATH_PRES_SCHEMA_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_PRES_SCHEMA_DEFINITIONS_EDIT)
    public String editDefinition(@PathVariable(PRES_SCHEMA_DEF_ID) Long id, Model model)
            throws PresentationSchemaDefinitionNotFoundException
    {
        PresentationSchemaDefinitionForm form = service.getDefinitionForEdit(id);
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_DEF_ID, id);
        model.addAttribute(MODEL_ATTR_SCHEMA_DEF_FORM, form);
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEFINITIONS, attributeDefinitionService.getDefinitions());
        return VIEW_PRES_SCHEMA_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_PRES_SCHEMA_DEFINITIONS_EDIT)
    public String editDefinitionSubmit(@PathVariable(PRES_SCHEMA_DEF_ID) Long id,
                                       @Valid @ModelAttribute(MODEL_ATTR_SCHEMA_DEF_FORM)
                                       PresentationSchemaDefinitionForm form,
                                       BindingResult bindingResult,
                                       Model model)
            throws PresentationSchemaDefinitionNotFoundException {
        if (bindingResult.hasErrors()) {
            return VIEW_PRES_SCHEMA_DEFINITIONS_FORM;
        }
        PresentationSchemaDefinitionDTO schemaDefinition = service.updateDefinition(id, form);
        model.addAttribute(PRES_SCHEMA_DEF_ID, schemaDefinition.getId());
        return "redirect:" + PATH_PRES_SCHEMA_DEFINITIONS_DETAIL;
    }

    @PostMapping(PATH_PRES_SCHEMA_DEFINITIONS_DELETE)
    public String deleteDefinition(@PathVariable(PRES_SCHEMA_DEF_ID) Long id, Model model)
            throws PresentationSchemaDefinitionNotFoundException
    {
        boolean success = service.deleteDefinition(id);
        if (!success) {
            model.addAttribute(PRES_SCHEMA_DEF_ID, id);
            model.addAttribute(MODEL_ATTR_DELETE_FAILED, true);
            return "redirect:" + PATH_PRES_SCHEMA_DEFINITIONS_DETAIL;
        } else {
            return "redirect:" + PATH_PRES_SCHEMA_DEFINITIONS;
        }
    }

}
