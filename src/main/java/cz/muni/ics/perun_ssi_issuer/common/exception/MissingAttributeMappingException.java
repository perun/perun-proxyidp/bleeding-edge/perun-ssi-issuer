package cz.muni.ics.perun_ssi_issuer.common.exception;

public class MissingAttributeMappingException extends RuntimeException {

    public MissingAttributeMappingException() {
        super();
    }

    public MissingAttributeMappingException(String message) {
        super(message);
    }

    public MissingAttributeMappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingAttributeMappingException(Throwable cause) {
        super(cause);
    }

    protected MissingAttributeMappingException(String message,
                                               Throwable cause,
                                               boolean enableSuppression,
                                               boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
