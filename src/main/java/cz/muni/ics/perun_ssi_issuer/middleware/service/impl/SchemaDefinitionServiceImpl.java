package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.SchemaAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.SchemaDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.Translations;
import cz.muni.ics.perun_ssi_issuer.middleware.service.SchemaDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaDefinitionForm;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("schemaDefinitionService")
@Slf4j
public class SchemaDefinitionServiceImpl implements SchemaDefinitionService {

    private final SchemaDefinitionFacade schemaDefinitionFacade;

    private final TranslationFacade translationFacade;

    @Autowired
    public SchemaDefinitionServiceImpl(@NonNull SchemaDefinitionFacade schemaDefinitionFacade,
                                       @NonNull TranslationFacade translationFacade)
    {
        this.schemaDefinitionFacade = schemaDefinitionFacade;
        this.translationFacade = translationFacade;
    }

    @Override
    public List<SchemaDefinitionDTO> getDefinitions() {
        List<SchemaDefinitionEntity> definitions = schemaDefinitionFacade.getDefinitions();
        return EntityMappingUtils.mapSchemaDefinitions(definitions);
    }

    @Override
    public SchemaDefinitionDTO getDefinition(@NonNull Long id) throws SchemaDefinitionNotFoundException {
        SchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        return EntityMappingUtils.map(definition);
    }

    @Override
    public SchemaDefinitionForm getDefinitionForEdit(@NonNull Long id) throws SchemaDefinitionNotFoundException {
        SchemaDefinitionForm form = new SchemaDefinitionForm();
        SchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        Translations translations = translationFacade.getSchemaDefinitionTranslations(definition.getName());

        Set<Long> attrDefIds = definition.getAttributeDefinitions()
                .stream()
                .map(SchemaAttributeDefinitionEntity::getId)
                .collect(Collectors.toSet());

        form.setName(definition.getName());
        form.setVersion(definition.getVersion());
        form.setAttributeDefinitions(attrDefIds);
        form.addNameTranslations(translations.getNameTranslations());
        form.addDescriptionTranslations(translations.getDescTranslations());

        return form;
    }

    @Transactional
    @Override
    public SchemaDefinitionDTO createDefinition(@NonNull SchemaDefinitionForm form)
            throws SchemaAlreadyExistsException
    {
        SchemaDefinitionEntity definition = SchemaDefinitionEntity.builder()
                .name(form.getName())
                .version(form.getVersion())
                .build();
        definition = schemaDefinitionFacade.createDefinition(definition, form.getAttributeDefinitions());
        translationFacade.createSchemaDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                form.getName()
        );
        return EntityMappingUtils.map(definition);
    }

    @Override
    @Transactional
    public SchemaDefinitionDTO updateDefinition(@NonNull Long id, @NonNull SchemaDefinitionForm form)
            throws SchemaDefinitionNotFoundException
    {
        SchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        String oldName = definition.getName();
        translationFacade.updateSchemaDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                form.getName(),
                oldName
        );
        return EntityMappingUtils.map(definition);
    }

    @Transactional
    @Override
    public boolean deleteDefinition(@NonNull Long id) throws SchemaDefinitionNotFoundException {
        SchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        translationFacade.deleteSchemaDefinitionTranslations(definition.getName());
        schemaDefinitionFacade.deleteDefinition(definition);
        return true;
    }

}
