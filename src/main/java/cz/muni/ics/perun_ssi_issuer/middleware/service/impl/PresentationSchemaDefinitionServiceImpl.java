package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationSchemaDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.Translations;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentationSchemaDefinitionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaDefinitionForm;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("presentationSchemaDefinitionService")
@Slf4j
public class PresentationSchemaDefinitionServiceImpl implements PresentationSchemaDefinitionService {

    private final PresentationSchemaDefinitionFacade schemaDefinitionFacade;

    private final TranslationFacade translationFacade;

    @Autowired
    public PresentationSchemaDefinitionServiceImpl(
            @NonNull PresentationSchemaDefinitionFacade presentationSchemaDefinitionFacade,
            @NonNull TranslationFacade translationFacade)
    {
        this.schemaDefinitionFacade = presentationSchemaDefinitionFacade;
        this.translationFacade = translationFacade;
    }

    @Override
    public List<PresentationSchemaDefinitionDTO> getDefinitions() {
        List<PresentationSchemaDefinitionEntity> definitions = schemaDefinitionFacade.getDefinitions();
        return EntityMappingUtils.mapPresentationSchemaDefinitions(definitions);
    }

    @Override
    public PresentationSchemaDefinitionDTO getDefinition(@NonNull Long id)
            throws PresentationSchemaDefinitionNotFoundException
    {
        PresentationSchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        return EntityMappingUtils.map(definition);
    }

    @Override
    public PresentationSchemaDefinitionForm getDefinitionForEdit(@NonNull Long id)
            throws PresentationSchemaDefinitionNotFoundException
    {
        PresentationSchemaDefinitionForm form = new PresentationSchemaDefinitionForm();
        PresentationSchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        Translations translations = translationFacade.getPresentationSchemaDefinitionTranslations(definition.getName());

        Set<Long> attrDefIds = definition.getAttributeDefinitions()
                .stream()
                .map(PresentationSchemaAttributeDefinitionEntity::getId)
                .collect(Collectors.toSet());

        form.setName(definition.getName());
        form.setSchemaId(definition.getSchemaDefinitionId());
        form.setAttributeDefinitions(attrDefIds);
        form.addNameTranslations(translations.getNameTranslations());
        form.addDescriptionTranslations(translations.getDescTranslations());

        return form;
    }

    @Transactional
    @Override
    public PresentationSchemaDefinitionDTO createDefinition(@NonNull PresentationSchemaDefinitionForm form) {
        PresentationSchemaDefinitionEntity definition = schemaDefinitionFacade.createDefinition(
                form, form.getAttributeDefinitions()
        );
        translationFacade.createPresentationSchemaDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                form.getName()
        );
        return EntityMappingUtils.map(definition);
    }

    @Override
    @Transactional
    public PresentationSchemaDefinitionDTO updateDefinition(@NonNull Long id, @NonNull PresentationSchemaDefinitionForm form)
            throws PresentationSchemaDefinitionNotFoundException
    {
        PresentationSchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        String oldName = definition.getName();
        translationFacade.updatePresentationSchemaDefinitionTranslations(
                new Translations(form.getNameTranslations(), form.getDescriptionTranslations()),
                form.getName(),
                oldName
        );
        return EntityMappingUtils.map(definition);
    }

    @Transactional
    @Override
    public boolean deleteDefinition(@NonNull Long id) throws PresentationSchemaDefinitionNotFoundException {
        PresentationSchemaDefinitionEntity definition = schemaDefinitionFacade.getDefinition(id);
        translationFacade.deletePresentationSchemaDefinitionTranslations(definition.getName());
        schemaDefinitionFacade.deleteDefinition(definition);
        return true;
    }

}
