package cz.muni.ics.perun_ssi_issuer.web.controller.admin;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentationSchemaAttributeDefinitionService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaAttributeDefinitionForm;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_CREATE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DELETE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_EDIT;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PRES_SCH_ATTR_DEF_ID;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;

@Controller
@Slf4j
public class PresentationSchemaAttributeDefinitionsController extends AppController {

    public static final String MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_FORM = "form";

    public static final String MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_ID = "schAttrDefId";

    public static final String MODEL_ATTR_PRES_SCHEMA_ATTR_DEFINITIONS = "definitions";

    public static final String MODEL_ATTR_PRES_SCHEMA_ATTR_DEFINITION = "definition";

    public static final String MODEL_ATTR_DELETE_FAILED = "deleteFailed";

    private final PresentationSchemaAttributeDefinitionService service;

    @Autowired
    public PresentationSchemaAttributeDefinitionsController(
            @NonNull ApplicationProperties applicationProperties,
            @NonNull OidcProperties oidcProperties,
            PresentationSchemaAttributeDefinitionService presentationSchemaAttributeDefinitionService)
    {
        super(applicationProperties, oidcProperties);
        this.service = presentationSchemaAttributeDefinitionService;
    }


    @GetMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS)
    public String definitions(Model model) {
        List<PresentationSchemaAttributeDefinitionDTO> schemaAttrDefinitions = service.getDefinitions();
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEFINITIONS, schemaAttrDefinitions);
        return VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS;
    }

    @GetMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL)
    public String definition(@PathVariable(PRES_SCH_ATTR_DEF_ID) Long schAttrDefId,
                             Model model) throws PresentationSchemaAttributeDefinitionNotFoundException {
        PresentationSchemaAttributeDefinitionDTO schemaAttrDefinition = service.getDefinition(schAttrDefId);
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEFINITION, schemaAttrDefinition);
        return VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_CREATE)
    public String createDefinition(@ModelAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_FORM)
                                   PresentationSchemaAttributeDefinitionForm form,
                                   Model model) {
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_ID, null);
        return VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_CREATE)
    public String createDefinitionSubmit(@Valid @ModelAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_FORM)
                                         PresentationSchemaAttributeDefinitionForm form,
                                         BindingResult bindingResult,
                                         Model model) {
        if (bindingResult.hasErrors()) {
            return VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
        }
        PresentationSchemaAttributeDefinitionDTO schemaAttrDefinition = service.createDefinition(form);
        model.addAttribute(PRES_SCH_ATTR_DEF_ID, schemaAttrDefinition.getId());
        return "redirect:" + PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_EDIT)
    public String editDefinition(@PathVariable(PRES_SCH_ATTR_DEF_ID) Long schAttrDefId,
                                 Model model)
            throws PresentationSchemaAttributeDefinitionNotFoundException {
        PresentationSchemaAttributeDefinitionForm form = service.getDefinitionForEdit(schAttrDefId);
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_ID, schAttrDefId);
        model.addAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_FORM, form);
        return VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_EDIT)
    public String editDefinitionSubmit(@PathVariable(PRES_SCH_ATTR_DEF_ID) Long schAttrDefId,
                                       @Valid @ModelAttribute(MODEL_ATTR_PRES_SCHEMA_ATTR_DEF_FORM)
                                       PresentationSchemaAttributeDefinitionForm form,
                                       BindingResult bindingResult,
                                       Model model)
            throws PresentationSchemaAttributeDefinitionNotFoundException {
        if (bindingResult.hasErrors()) {
            return VIEW_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
        }
        PresentationSchemaAttributeDefinitionDTO schemaAttrDefinition = service.updateDefinition(schAttrDefId, form);
        model.addAttribute(PRES_SCH_ATTR_DEF_ID, schemaAttrDefinition.getId());
        return "redirect:" + PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
    }

    @PostMapping(PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DELETE)
    public String deleteDefinition(@PathVariable(PRES_SCH_ATTR_DEF_ID) Long schAttrDefId,
                                   Model model)
            throws PresentationSchemaAttributeDefinitionNotFoundException {
        boolean success = service.deleteDefinition(schAttrDefId);
        if (!success) {
            model.addAttribute(PRES_SCH_ATTR_DEF_ID, schAttrDefId);
            model.addAttribute(MODEL_ATTR_DELETE_FAILED, true);
            return "redirect:" + PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
        } else {
            return "redirect:" + PATH_PRES_SCHEMA_ATTRIBUTE_DEFINITIONS;
        }
    }

}
