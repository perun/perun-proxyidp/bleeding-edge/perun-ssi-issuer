package cz.muni.ics.perun_ssi_issuer.common.exception;

public class UnauthorizedConnectionAccessException extends Exception {

    public UnauthorizedConnectionAccessException() {
        super();
    }

    public UnauthorizedConnectionAccessException(String message) {
        super(message);
    }

    public UnauthorizedConnectionAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedConnectionAccessException(Throwable cause) {
        super(cause);
    }

    protected UnauthorizedConnectionAccessException(String message, Throwable cause,
                                                    boolean enableSuppression,
                                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
