package cz.muni.ics.perun_ssi_issuer.data.perun.rpc;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import cz.muni.ics.perun_ssi_issuer.data.perun.enums.MemberStatus;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.Attribute;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.AttributeDefinition;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.ExtSource;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.Group;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.Member;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.User;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.UserExtSource;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * This class is mapping JsonNodes to object models.
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Slf4j
public class PerunEntityMapper {

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String VO_ID = "voId";

    public static final String USER_ID = "userId";

    public static final String STATUS = "status";

    public static final String TYPE = "type";

    public static final String LOGIN = "login";

    public static final String EXT_SOURCE = "extSource";

    public static final String LAST_ACCESS = "lastAccess";

    public static final String FRIENDLY_NAME = "friendlyName";

    public static final String NAMESPACE = "namespace";

    public static final String ENTITY = "entity";

    public static final String VALUE = "value";

    public static final String FIRST_NAME = "firstName";

    public static final String LAST_NAME = "lastName";

    public static final String MIDDLE_NAME = "middleName";

    public static final String TITLE_BEFORE = "titleBefore";

    public static final String TITLE_AFTER = "titleAfter";

    public static final String VALUE_CREATED_AT = "valueCreatedAt";

    public static final String VALUE_MODIFIED_AT = "valueModifiedAt";


    /**
     * Maps JsonNode to Group model.
     *
     * @param json Group in JSON format from Perun to be mapped.
     * @return Mapped Group object.
     */
    public static Group mapGroup(JsonNode json) {
        String[] requiredParams = new String[]{ID, NAME, VO_ID};
        if (!hasRequiredFields(json, requiredParams)) {
            return null;
        }

        Long id = json.get(ID).longValue();
        String name = json.get(NAME).textValue();
        Long voId = json.get(VO_ID).longValue();

        return new Group(id, name, voId, null);
    }

    /**
     * Maps JsonNode to List of Groups.
     *
     * @param json JSON array of groups in JSON format from Perun to be mapped.
     * @return List of groups.
     */
    public static List<Group> mapGroups(JsonNode json) {
        if (isEmptyJsonArray(json)) {
            return new ArrayList<>();
        }

        List<Group> result = new ArrayList<>();
        for (JsonNode entry : json) {
            Group mappedGroup = PerunEntityMapper.mapGroup(entry);
            result.add(mappedGroup);
        }

        return result;
    }

    /**
     * Maps JsonNode to Member model.
     *
     * @param json Member in JSON format from Perun to be mapped.
     * @return Mapped Member object.
     */
    public static Member mapMember(JsonNode json) {
        String[] requiredParams = new String[]{ID, USER_ID, VO_ID, STATUS};
        if (!hasRequiredFields(json, requiredParams)) {
            return null;
        }

        Long id = json.get(ID).longValue();
        Long userId = json.get(USER_ID).longValue();
        Long voId = json.get(VO_ID).longValue();
        MemberStatus status = MemberStatus.valueOf(json.get(STATUS).textValue());

        return new Member(id, userId, voId, status);
    }

    public static List<Member> mapMembers(JsonNode json) {
        if (isEmptyJsonArray(json)) {
            return new ArrayList<>();
        }

        List<Member> members = new ArrayList<>();
        for (JsonNode entry : json) {
            Member mappedMember = PerunEntityMapper.mapMember(entry);
            members.add(mappedMember);
        }

        return members;
    }

    /**
     * Maps JsonNode to ExtSource model.
     *
     * @param json ExtSource in JSON format from Perun to be mapped.
     * @return Mapped ExtSource object.
     */
    public static ExtSource mapExtSource(JsonNode json) {
        String[] requiredParams = new String[]{ID, NAME, TYPE};
        if (!hasRequiredFields(json, requiredParams)) {
            return null;
        }

        Long id = json.get(ID).longValue();
        String name = json.get(NAME).textValue();
        String type = json.get(TYPE).textValue();

        return new ExtSource(id, name, type);
    }

    /**
     * Maps JsonNode to UserExtSource model.
     *
     * @param json UserExtSource in JSON format from Perun to be mapped.
     * @return Mapped UserExtSource object.
     */
    public static UserExtSource mapUserExtSource(JsonNode json) {
        String[] requiredParams = new String[]{ID, LOGIN, EXT_SOURCE, LAST_ACCESS};
        if (!hasRequiredFields(json, requiredParams)) {
            return null;
        }

        Long id = json.get(ID).longValue();
        String login = json.get(LOGIN).textValue();
        ExtSource extSource = mapExtSource(json.get(EXT_SOURCE));
        Timestamp lastAccess = Timestamp.valueOf(json.get(LAST_ACCESS).textValue());

        return new UserExtSource(id, extSource, login, lastAccess);
    }

    /**
     * Maps JsonNode to List of UserExtSources.
     *
     * @param json JSON array of userExtSources in JSON format from Perun to be mapped.
     * @return List of userExtSources.
     */
    public static List<UserExtSource> mapUserExtSources(JsonNode json) {
        if (isEmptyJsonArray(json)) {
            return new ArrayList<>();
        }

        List<UserExtSource> userExtSources = new ArrayList<>();
        for (JsonNode entry : json) {
            UserExtSource mappedUes = PerunEntityMapper.mapUserExtSource(entry);
            userExtSources.add(mappedUes);
        }

        return userExtSources;
    }

    public static AttributeDefinition mapAttributeDefinition(JsonNode json) {
        String[] requiredParams = new String[]{ID, FRIENDLY_NAME, NAMESPACE, TYPE, ENTITY};
        if (!hasRequiredFields(json, requiredParams)) {
            return null;
        }

        Long id = json.get(ID).asLong();
        String friendlyName = json.get(FRIENDLY_NAME).textValue();
        String namespace = json.get(NAMESPACE).textValue();
        String type = json.get(TYPE).textValue();
        String entity = json.get(ENTITY).textValue();

        return new AttributeDefinition(id, friendlyName, namespace, type, entity);
    }

    public static Attribute mapAttribute(JsonNode json) {
        AttributeDefinition definition = mapAttributeDefinition(json);
        if (definition == null) {
            return null;
        }
        JsonNode value = json.hasNonNull(VALUE) ?
                json.get(VALUE) : JsonNodeFactory.instance.nullNode();

        Timestamp valueCreatedAt = null;
        if (json.hasNonNull(VALUE_CREATED_AT)) {
            String valueCreatedAtStr = json.get(VALUE_CREATED_AT).asText();
            valueCreatedAt = Timestamp.valueOf(valueCreatedAtStr);
        }
        Timestamp valueModifiedAt = null;
        if (json.hasNonNull(VALUE_MODIFIED_AT)) {
            String valueModifiedAtStr = json.get(VALUE_MODIFIED_AT).asText();
            valueModifiedAt = Timestamp.valueOf(valueModifiedAtStr);
        }
        return new Attribute(definition, value, valueCreatedAt, valueModifiedAt);
    }

    public static List<Attribute> mapAttributes(JsonNode json) {
        if (isEmptyJsonArray(json)) {
            return new ArrayList<>();
        }
        List<Attribute> attributes = new ArrayList<>();
        for (JsonNode entryNode : json) {
            Attribute a = mapAttribute(entryNode);
            if (a == null) {
                log.warn("Failed to map Attribute from json '{}'", entryNode);
                continue;
            }
            attributes.add(a);
        }

        return attributes;
    }

    public static User mapUser(@NonNull JsonNode json) {
        String[] requiredParams = new String[]{ID, LAST_NAME};
        if (!hasRequiredFields(json, requiredParams)) {
            return null;
        }

        Long id = json.get(ID).asLong();
        String lastName = json.get(LAST_NAME).textValue();

        String firstName = json.hasNonNull(FIRST_NAME) ?
                json.get(FIRST_NAME).textValue() : null;
        String middleName = json.hasNonNull(MIDDLE_NAME) ?
                json.get(MIDDLE_NAME).textValue() : null;
        String titleBefore = json.hasNonNull(TITLE_BEFORE) ?
                json.get(TITLE_BEFORE).textValue() : null;
        String titleAfter = json.hasNonNull(TITLE_AFTER) ?
                json.get(TITLE_AFTER).textValue() : null;

        return new User(id, firstName, middleName, lastName, titleBefore, titleAfter);
    }

    public static List<User> mapUsers(JsonNode json) {
        if (isEmptyJsonArray(json)) {
            return new ArrayList<>();
        }

        List<User> users = new ArrayList<>();
        for (JsonNode entry : json) {
            User mappedUser = PerunEntityMapper.mapUser(entry);
            users.add(mappedUser);
        }

        return users;
    }


    private static boolean hasRequiredFields(JsonNode json, @NonNull String[] params) {
        if (isEmptyJsonObject(json)) {
            return false;
        }

        for (String param : params) {
            if (!json.hasNonNull(param)) {
                return false;
            }
        }

        return true;
    }

    private static boolean isEmptyJsonArray(JsonNode json) {
        return json == null || json.isNull() || json.isEmpty() || !json.isArray();
    }

    private static boolean isEmptyJsonObject(JsonNode json) {
        return json == null || json.isNull() || json.isEmpty() || !json.isObject();
    }

}
