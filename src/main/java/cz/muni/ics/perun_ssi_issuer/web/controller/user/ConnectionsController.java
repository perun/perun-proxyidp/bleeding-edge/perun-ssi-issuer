package cz.muni.ics.perun_ssi_issuer.web.controller.user;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialConnectionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialCredentialDefinitionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.ConnectionService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.CredentialService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.InvitationService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.exceptions.BadRequestParameterException;
import cz.muni.ics.perun_ssi_issuer.web.exceptions.NotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.InvitationDTO;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.CONNECTION_ID;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.CREDENTIAL_DEF_ID;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.CREDENTIAL_ID;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CONNECTIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CONNECTIONS_DEACTIVATE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CONNECTIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIALS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIALS_ISSUE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIALS_PROCESSED;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CREDENTIALS_REVOKE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_ISSUE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGIN;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CONNECTIONS;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CONNECTION_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CREDENTIAL_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CREDENTIAL_ISSUE;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_CREDENTIAL_PROCESSED;

@Controller
@Slf4j
public class ConnectionsController extends AppController {

    // === CONSTANTS - REQUEST PARAMS === //

    public static final String REQUEST_PARAM_CREDENTIAL_ID = "credentialId";


    // === CONSTANTS - MODEL ATTRS === //

    public static final String MODEL_ATTR_CONNECTIONS = "connections";

    public static final String MODEL_ATTR_INVITATIONS = "invitations";

    public static final String MODEL_ATTR_CONNECTION = "connection";

    public static final String MODEL_ATTR_CREDENTIAL = "credential";

    public static final String MODEL_ATTR_CONNECTION_ID = "connectionId";

    public static final String MODEL_ATTR_CREDENTIAL_ID = "credentialId";
    public static final String MODEL_ATTR_CREDENTIAL_DEFINITION_ID = "credentialDefinitionId";

    public static final String MODEL_ATTR_CREDENTIAL_ACCEPTED = "accepted";

    // === CLASS FIELDS === //

    private final ConnectionService connectionService;

    private final InvitationService invitationService;

    private final CredentialService credentialService;

    // === CONSTRUCTORS === //

    @Autowired
    public ConnectionsController(@NonNull ApplicationProperties applicationProperties,
                                 @NonNull OidcProperties oidcProperties,
                                 @NonNull ConnectionService connectionService,
                                 @NonNull InvitationService invitationService,
                                 @NonNull CredentialService credentialService) {
        super(applicationProperties, oidcProperties);
        this.connectionService = connectionService;
        this.invitationService = invitationService;
        this.credentialService = credentialService;
    }

    // === CONNECTIONS PART === //

    @GetMapping(path = {"/", PATH_CONNECTIONS})
    public String connectionsOverview(Model model, Authentication auth)
            throws QRCodeGeneratorError, NoUserDetailsAvailableException
    {
        List<ConnectionDTO> connections;
        List<InvitationDTO> invitations;
        try {
            connections = connectionService.getConnections(auth);
            invitations = invitationService.getInvitations(auth);
        } catch (UserNotFoundException ex) {
            SecurityContextHolder.getContext().setAuthentication(null);
            SecurityContextHolder.clearContext();
            return "redirect:" + PATH_LOGIN;
        }

        model.addAttribute(MODEL_ATTR_CONNECTIONS, connections);
        model.addAttribute(MODEL_ATTR_INVITATIONS, invitations);

        return VIEW_CONNECTIONS;
    }

    @GetMapping(path = PATH_CONNECTIONS_DETAIL)
    public String connectionDetail(@PathVariable(name = CONNECTION_ID) Long id,
                                   Model model,
                                   Authentication auth)
            throws BadRequestParameterException, NotFoundException,
            ConnectionNotFoundException, UnauthorizedConnectionAccessException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException
    {
        if (id == null) {
            throw new BadRequestParameterException("Invalid ID passed");
        }

        ConnectionDTO conn = connectionService.getConnection(id, auth);
        if (conn == null) {
            log.error("Principal '{}' tried to access non-existing connection detail '{}'",
                    auth.getPrincipal(), id);
            throw new NotFoundException();
        }

        model.addAttribute(MODEL_ATTR_CONNECTION, conn);

        return VIEW_CONNECTION_DETAIL;
    }

    @PostMapping(path = PATH_CONNECTIONS_DEACTIVATE)
    public String connectionDeactivate(@PathVariable(CONNECTION_ID) Long connId,
                                       Model model,
                                       Authentication auth)
    {
        //TODO: implement - remove connection, revoke all credentials issued for it?
        return "redirect:" + PATH_CONNECTIONS;
    }

    // === CREDENTIALS PART === //

    @GetMapping(path = PATH_CREDENTIALS_ISSUE)
    public String issueCredential(@PathVariable(CONNECTION_ID) Long connId,
                                  @PathVariable(CREDENTIAL_DEF_ID) Long credDefId,
                                  Model model,
                                  Authentication auth)
            throws NoUserDetailsAvailableException,
            DatabaseInconsistencyException, UnauthorizedConnectionAccessException,
            PerunUserNotFoundException, BadRequestParameterException, NotFoundException
    {
        if (connId == null) {
            throw new BadRequestParameterException("Invalid connection ID passed");
        } else if (credDefId == null) {
            throw new BadRequestParameterException("Invalid credential definition ID passed");
        }

        CredentialDTO credential;
        try {
            credential = credentialService.issueCredential(connId, credDefId, auth);
        } catch (ConnectionNotFoundException
                 | CredentialDefinitionNotFoundException
                 | UserNotFoundException ex) {
            throw new NotFoundException(ex);
        }

        if (credential == null) {
            throw new NotFoundException();
        }

        model.addAttribute(MODEL_ATTR_CREDENTIAL, credential);

        return VIEW_CREDENTIAL_ISSUE;
    }

    @PostMapping(path = PATH_CREDENTIALS_ISSUE)
    public String issueCredential(@PathVariable(CONNECTION_ID) Long connId,
                                  @PathVariable(CREDENTIAL_DEF_ID) Long credDefId,
                                  @RequestParam(REQUEST_PARAM_CREDENTIAL_ID) Long credentialId,
                                  Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, CredentialCredentialDefinitionMismatchException,
            CredentialConnectionMismatchException, CredentialNotFoundException
    {
        CredentialDTO credentialDTO = credentialService.getCredentialCheckStatus(connId, credDefId, credentialId, auth);
        if (credentialDTO == null) {
            return "redirect:" + PATH_CONNECTIONS + '/' + connId + PATH_ISSUE + '/' + credDefId;
        }
        boolean processed = credentialDTO.isProcessed();
        if (!processed) {
            return "redirect:" + PATH_CONNECTIONS + '/' + connId + PATH_ISSUE + '/' + credDefId;
        } else {
            return "redirect:" + PATH_CONNECTIONS + '/' + connId + PATH_ISSUE + '/' + credDefId + '/' + credentialId;
        }
    }

    @GetMapping(path = PATH_CREDENTIALS_PROCESSED)
    public String credentialProcessed(@PathVariable(CONNECTION_ID) Long connId,
                                      @PathVariable(CREDENTIAL_DEF_ID) Long credDefId,
                                      @PathVariable(CREDENTIAL_ID) Long credId,
                                      Authentication auth,
                                      Model model)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, CredentialCredentialDefinitionMismatchException,
            CredentialConnectionMismatchException, CredentialNotFoundException
    {
        CredentialDTO credentialDTO = credentialService.getCredential(connId, credDefId, credId, auth);
        model.addAttribute(MODEL_ATTR_CONNECTION_ID, connId);
        model.addAttribute(MODEL_ATTR_CREDENTIAL_ACCEPTED, credentialDTO.isAccepted());
        if (credentialDTO.isAccepted()) {
            model.addAttribute(MODEL_ATTR_CREDENTIAL_ID, credentialDTO.getId());
        } else {
            model.addAttribute(MODEL_ATTR_CREDENTIAL_DEFINITION_ID, credDefId);
        }
        return VIEW_CREDENTIAL_PROCESSED;
    }

    @GetMapping(path = PATH_CREDENTIALS_DETAIL)
    public String credentialDetail(@PathVariable(CONNECTION_ID) Long connId,
                                   @PathVariable(CREDENTIAL_ID) Long credId,
                                   Model model,
                                   Authentication auth)
            throws ConnectionNotFoundException, NoUserDetailsAvailableException,
            CredentialConnectionMismatchException, CredentialNotFoundException,
            DatabaseInconsistencyException, UnauthorizedConnectionAccessException
    {
        CredentialDTO credentialDTO = credentialService.getCredentialForDetail(connId, credId, auth);
        model.addAttribute(MODEL_ATTR_CREDENTIAL, credentialDTO);
        return VIEW_CREDENTIAL_DETAIL;
    }

    @RequestMapping(path = PATH_CREDENTIALS_REVOKE)
    public String credentialRevoke(@PathVariable(CONNECTION_ID) Long connId,
                                   @PathVariable(CREDENTIAL_ID) Long credentialId,
                                   Model model,
                                   Authentication auth)
    {
        //TODO: implement - revoke credential
        model.addAttribute(CONNECTION_ID, connId);
        return "redirect:" + PATH_CONNECTIONS_DETAIL;
    }
    // === PRIVATE METHODS === //

}
