package cz.muni.ics.perun_ssi_issuer.web.configuration;

import cz.muni.ics.perun_ssi_issuer.data.aries.properties.AriesAgentProperties;
import cz.muni.ics.perun_ssi_issuer.web.ApiKeyAuthenticationManager;
import cz.muni.ics.perun_ssi_issuer.web.ApiKeyAuthenticationProcessingFilter;
import cz.muni.ics.perun_ssi_issuer.web.RecordUpdatingSavedRequestAwareAuthnSuccessHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.ERROR_PATHS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGIN;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGIN_ERROR;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGOUT;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGOUT_COMPLETE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PUBLIC_PATHS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.RESOURCE_PATHS;
import static cz.muni.ics.perun_ssi_issuer.web.controller.WebhookEndpointController.PATH_WEBHOOK_BASE;

@EnableWebSecurity
@Configuration
@Slf4j
public class SecurityConfiguration {

    public static final String ADMIN = "ADMIN";

    public static final String USER = "USER";

    public static final SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_" + ADMIN);

    public static final SimpleGrantedAuthority ROLE_USER = new SimpleGrantedAuthority("ROLE_" + USER);

    @Bean
    @Order(1)
    public SecurityFilterChain webhookSecurityFilterChain(HttpSecurity http,
                                                          AriesAgentProperties ariesAgentProperties)
            throws Exception {
        ApiKeyAuthenticationProcessingFilter filter = new ApiKeyAuthenticationProcessingFilter();
        AuthenticationManager authenticationManager = new ApiKeyAuthenticationManager(ariesAgentProperties);
        filter.setAuthenticationManager(authenticationManager);

        AntPathRequestMatcher requestMatcher = new AntPathRequestMatcher(
                PATH_WEBHOOK_BASE + "/**");

        http
                .requestMatcher(requestMatcher)
                .httpBasic().disable()
                .csrf().disable()
                .formLogin().disable()
                .logout().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .addFilter(filter)
                .authorizeRequests()
                .requestMatchers(requestMatcher).authenticated();
        return http.build();
    }


    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authenticationConfiguration)
            throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    @Order(2)
    public SecurityFilterChain webpagesSecurityFilterChain(HttpSecurity http) throws Exception {
        http
                .antMatcher("/**")
                .csrf().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
                .authorizeRequests()
                .antMatchers(RESOURCE_PATHS).permitAll()
                .antMatchers(PUBLIC_PATHS).permitAll()
                .antMatchers(ERROR_PATHS).permitAll()
                .anyRequest().authenticated()
                .and()
                .oauth2Login(o -> {
                    o.loginPage(PATH_LOGIN);
                    o.successHandler(oauthSuccessHandler());
                    o.failureHandler(oauthFailureHandler());
                })
                .logout(
                        l -> {
                            l.clearAuthentication(true);
                            l.invalidateHttpSession(true);
                            l.logoutUrl(PATH_LOGOUT);
                            l.logoutSuccessUrl(PATH_LOGOUT_COMPLETE).permitAll();
                        }
                );
        return http.build();
    }

    @Bean
    public SavedRequestAwareAuthenticationSuccessHandler oauthSuccessHandler() {
        return new RecordUpdatingSavedRequestAwareAuthnSuccessHandler();
    }

    @Bean
    public SimpleUrlAuthenticationFailureHandler oauthFailureHandler() {
        return new SimpleUrlAuthenticationFailureHandler(PATH_LOGIN_ERROR);
    }

}
