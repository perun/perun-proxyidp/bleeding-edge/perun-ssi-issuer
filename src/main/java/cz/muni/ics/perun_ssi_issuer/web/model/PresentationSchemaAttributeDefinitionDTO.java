package cz.muni.ics.perun_ssi_issuer.web.model;

import cz.muni.ics.perun_ssi_issuer.common.enums.SchemaAttributeType;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.references.PresentationSchemaDefinitionReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class PresentationSchemaAttributeDefinitionDTO extends DefinitionLocalization {

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String mimeType;

    @NotNull
    private SchemaAttributeType attributeType;

    @NotBlank
    private String perunRpcAttribute;

    @NotNull
    @Builder.Default
    private Set<PresentationSchemaDefinitionReference> schemaDefinitionReferences = new HashSet<>();

    @Override
    public String i18nNameKey() {
        return TranslationFacade.getPresSchAttrDefI18nNameKey(getName());
    }

    @Override
    public String i18nDescKey() {
        return TranslationFacade.getPresSchAttrDefI18nDescKey(getName());
    }

}
