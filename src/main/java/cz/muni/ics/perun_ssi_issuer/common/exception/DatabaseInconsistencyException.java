package cz.muni.ics.perun_ssi_issuer.common.exception;

public class DatabaseInconsistencyException extends Exception {

    public DatabaseInconsistencyException() {
        super();
    }

    public DatabaseInconsistencyException(String message) {
        super(message);
    }

    public DatabaseInconsistencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseInconsistencyException(Throwable cause) {
        super(cause);
    }

    protected DatabaseInconsistencyException(String message, Throwable cause,
                                             boolean enableSuppression,
                                             boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
