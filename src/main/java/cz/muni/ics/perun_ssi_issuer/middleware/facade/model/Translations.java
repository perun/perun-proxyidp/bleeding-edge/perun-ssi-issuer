package cz.muni.ics.perun_ssi_issuer.middleware.facade.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Translations {

    private Map<String, String> nameTranslations;

    private Map<String, String> descTranslations;

}
