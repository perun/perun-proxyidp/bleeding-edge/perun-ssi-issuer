--
-- OPERATION TABLES
--

CREATE TABLE translations
(
    id                  BIGINT AUTO_INCREMENT NOT NULL,
    locale              VARCHAR(2)            NOT NULL,
    msg_key             VARCHAR(255)          NOT NULL,
    content             TEXT                  NOT NULL,
    CONSTRAINT pk_languages PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE schema_attribute_definitions
(
    id                  BIGINT AUTO_INCREMENT NOT NULL,
    name                VARCHAR(255)          NOT NULL,
    mime_type           VARCHAR(255)          NULL,
    attribute_type      INT                   NOT NULL,
    perun_attribute     VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_schema_attribute_definitions PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE schema_definition_schema_attribute_definition
(
    schema_attribute_definition_id BIGINT NOT NULL,
    schema_definition_id           BIGINT NOT NULL
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE schema_definitions
(
    id                   BIGINT AUTO_INCREMENT NOT NULL,
    schema_definition_id VARCHAR(255)          NOT NULL,
    name                 VARCHAR(255)          NOT NULL,
    version              VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_schema_definitions PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE credential_definitions
(
    id                       BIGINT AUTO_INCREMENT NOT NULL,
    credential_definition_id VARCHAR(255)          NOT NULL,
    support_revocation       BIT(1)                NOT NULL,
    schema_id                BIGINT                NOT NULL,
    CONSTRAINT pk_credential_definitions PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

--
-- USER RELATED
--

CREATE TABLE users
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    identifier VARCHAR(255)          NOT NULL,
    name       VARCHAR(255)          NOT NULL,
    email      VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE invitations
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    connection_id VARCHAR(255)          NOT NULL,
    alias         VARCHAR(255)          NOT NULL,
    link          TEXT                  NOT NULL,
    pending       BIT(1)                NOT NULL,
    user_id       BIGINT                NOT NULL,
    CONSTRAINT pk_invitations PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE connections
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    connection_id VARCHAR(255)          NOT NULL,
    alias         VARCHAR(255)          NOT NULL,
    status        INT                   NOT NULL,
    user_id       BIGINT                NOT NULL,
    updated_at    DATETIME              NOT NULL,
    CONSTRAINT pk_connections PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE credentials
(
    id                       BIGINT AUTO_INCREMENT NOT NULL,
    credential_id            VARCHAR(255)          NOT NULL,
    attributes               VARCHAR(255)          NOT NULL,
    processed                BIT(1)                NOT NULL,
    accepted                 BIT(1)                NOT NULL,
    active                   BIT(1)                NOT NULL,
    connection_id            BIGINT                NOT NULL,
    credential_definition_id BIGINT                NOT NULL,
    updated_at               DATETIME              NOT NULL,
    CONSTRAINT pk_credentials PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE pres_schema_definition_pres_schema_attribute_definition
(
    pres_schema_attribute_definition_id BIGINT NOT NULL,
    pres_schema_definition_id           BIGINT NOT NULL,
    CONSTRAINT pk_pres_schema_definition_pres_schema_attribute_definition
        PRIMARY KEY (pres_schema_attribute_definition_id, pres_schema_definition_id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE presentation_schema_definitions
(
    id                   BIGINT AUTO_INCREMENT NOT NULL,
    name                 VARCHAR(255)          NOT NULL,
    schema_definition_id VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_presentation_schema_definitions PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE pres_schema_attribute_definitions
(
    id              BIGINT AUTO_INCREMENT NOT NULL,
    name            VARCHAR(255)          NOT NULL,
    attribute_type  INT                   NOT NULL,
    CONSTRAINT pk_pres_schema_attribute_definitions PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE presentation_exchanges
(
    id                       BIGINT AUTO_INCREMENT NOT NULL,
    presentation_exchange_id VARCHAR(255)          NOT NULL,
    connection_id            BIGINT                NOT NULL,
    schema_id                BIGINT                NOT NULL,
    cred_exch_content_id     BIGINT,
    completed                BOOLEAN               NOT NULL DEFAULT FALSE,
    CONSTRAINT pk_presentation_exchanges PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

CREATE TABLE presentation_exchange_contents
(
    id                   BIGINT AUTO_INCREMENT NOT NULL,
    presentation_exch_id BIGINT                NOT NULL,
    content              BLOB                  NOT NULL,
    CONSTRAINT pk_presentation_exchange_contents PRIMARY KEY (id)
) COLLATE = utf8mb4_unicode_ci;

--
-- UNIQUE CONSTRAINTS
--

ALTER TABLE pres_schema_attribute_definitions
    ADD CONSTRAINT constr_unique_pres_sch_attr_defs_name
        UNIQUE (name);

ALTER TABLE presentation_schema_definitions
    ADD CONSTRAINT constr_unique_pres_schema_defs_schema_definition_id
        UNIQUE (schema_definition_id);

ALTER TABLE presentation_exchange_contents
    ADD CONSTRAINT constr_unique_pres_exch_id
        UNIQUE (presentation_exch_id);

ALTER TABLE presentation_exchanges
    ADD CONSTRAINT constr_unique_pres_exch_id UNIQUE (presentation_exchange_id);

ALTER TABLE credentials
    ADD CONSTRAINT constr_unique_credentials_credential_id
        UNIQUE (credential_id);

ALTER TABLE schema_attribute_definitions
    ADD CONSTRAINT constr_unique_sch_attr_defs_name
        UNIQUE (name);

ALTER TABLE users
    ADD CONSTRAINT constr_unique_users_identifier
        UNIQUE (identifier);

ALTER TABLE invitations
    ADD CONSTRAINT constr_unique_invitations_connection_id
        UNIQUE (connection_id);

ALTER TABLE connections
    ADD CONSTRAINT constr_unique_connections_connection_id
        UNIQUE (connection_id);

ALTER TABLE credential_definitions
    ADD CONSTRAINT constr_unique_cred_defs_cred_def_id
        UNIQUE (credential_definition_id);

ALTER TABLE schema_definitions
    ADD CONSTRAINT constr_unique_schema_defs_name_version
        UNIQUE (name, version);

ALTER TABLE schema_definitions
    ADD CONSTRAINT constr_unique_schema_defs_schema_definition_id
        UNIQUE (schema_definition_id);

--
-- FOREIGN KEYS CONSTRAINTS
--

ALTER TABLE credential_definitions
    ADD CONSTRAINT FK_CREDENTIAL_DEFINITIONS_ON_SCHEMA
        FOREIGN KEY (schema_id) REFERENCES schema_definitions (id);

ALTER TABLE invitations
    ADD CONSTRAINT FK_INVITATIONS_ON_USER
        FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE connections
    ADD CONSTRAINT FK_CONNECTIONS_ON_USER
        FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE schema_definition_schema_attribute_definition
    ADD CONSTRAINT FK_SCH_DEFS_SCH_ATTR_DEFS_ON_SCH_ATTR_DEFS
        FOREIGN KEY (schema_attribute_definition_id) REFERENCES schema_attribute_definitions (id);

ALTER TABLE schema_definition_schema_attribute_definition
    ADD CONSTRAINT FK_SCH_DEFS_SCH_ATTR_DEFS_ON_SCH_DEFS
        FOREIGN KEY (schema_definition_id) REFERENCES schema_definitions (id);

ALTER TABLE presentation_exchanges
    ADD CONSTRAINT FK_PRESENTATION_EXCHANGES_ON_CONNECTION
        FOREIGN KEY (connection_id) REFERENCES connections (id);

ALTER TABLE presentation_exchanges
    ADD CONSTRAINT FK_PRESENTATION_EXCHANGES_ON_CRED_EXCH_CONTENT
        FOREIGN KEY (cred_exch_content_id) REFERENCES presentation_exchange_contents (id);

ALTER TABLE presentation_exchanges
    ADD CONSTRAINT FK_PRESENTATION_EXCHANGES_ON_SCHEMA
        FOREIGN KEY (schema_id) REFERENCES presentation_schema_definitions (id);

ALTER TABLE credentials
    ADD CONSTRAINT FK_CREDENTIALS_ON_CONNECTION
        FOREIGN KEY (connection_id) REFERENCES connections (id);

ALTER TABLE credentials
    ADD CONSTRAINT FK_CREDENTIALS_ON_CREDENTIAL_DEFINITION
        FOREIGN KEY (credential_definition_id) REFERENCES credential_definitions (id);

ALTER TABLE presentation_exchange_contents
    ADD CONSTRAINT FK_PRESENTATION_EXCHANGE_CONTENTS_ON_PRESENTATION_EXCH
        FOREIGN KEY (presentation_exch_id) REFERENCES presentation_exchanges (id);

ALTER TABLE pres_schema_definition_pres_schema_attribute_definition
    ADD CONSTRAINT FK_PRES_SCH_DEFS_PRES_SCH_ATTR_DEFS_ON_PRES_SCH_ATTR_DEFS
        FOREIGN KEY (pres_schema_attribute_definition_id) REFERENCES pres_schema_attribute_definitions (id);

ALTER TABLE pres_schema_definition_pres_schema_attribute_definition
    ADD CONSTRAINT FK_PRES_SCH_DEFS_PRES_SCH_ATTR_DEFS_ON_PRES_SCH_DEFS
        FOREIGN KEY (pres_schema_definition_id) REFERENCES presentation_schema_definitions (id);

--
-- INDICES
--
CREATE INDEX idx_credentials_conn_id_cred_def_id ON credentials (connection_id, credential_definition_id);

CREATE UNIQUE INDEX idx_sch_attr_defs_name ON schema_attribute_definitions (name);
CREATE UNIQUE INDEX idx_schema_defs_name_version ON schema_definitions (name, version);
CREATE UNIQUE INDEX idx_schema_defs_schema_definition_id ON schema_definitions (schema_definition_id);
CREATE UNIQUE INDEX idx_cred_defs_cred_def_id ON credential_definitions (credential_definition_id);
CREATE UNIQUE INDEX idx_users_identifier ON users (identifier);
CREATE UNIQUE INDEX idx_invitations_connection_id ON invitations (connection_id);
CREATE UNIQUE INDEX idx_connections_connection_id ON connections (connection_id);
CREATE UNIQUE INDEX idx_credentials_credential_id ON credentials (credential_id);
CREATE UNIQUE INDEX idx_pres_schema_defs_schema_definition_id ON presentation_schema_definitions (schema_definition_id);
CREATE UNIQUE INDEX idx_pres_sch_attr_defs_name ON pres_schema_attribute_definitions (name);
CREATE UNIQUE INDEX idx_pres_exch ON presentation_exchange_contents (presentation_exch_id);
CREATE UNIQUE INDEX idx_pres_exch ON presentation_exchanges (presentation_exchange_id);
