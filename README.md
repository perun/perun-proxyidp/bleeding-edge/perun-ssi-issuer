# Perun SSI Worker

This application acts as a controller of the Aries Cloud Agent Python (ACApy). It provides functionality to configure
and issue verifiable credentials as well as make verifiable presentations.

### Configuration

The configuration is located in a standard Spring loaded file. Example file follows.

```yaml
---

spring:
  mvc.ifnore-default-mode-on-redirect: true
  profiles:
    dev
  main:
    allow-bean-definition-overriding: true
  # Database connection
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    url: jdbc:mysql://localhost:3306/ssi_issuer
    username: username
    password: password
    hikari:
      minimumIdle: 2
      maximumPoolSize: 20
      idleTimeout: 30000
      poolName: ssi_worker_pool
      maxLifetime: 2000000
      connectionTimeout: 30000
  jpa:
    database-platform: org.hibernate.dialect.MySQLDialect
    hibernate:
      ddl-auto: validate
    show-sql: true

server:
  error:
    path: "/error"

logging:
  level:
    org.springframework: info
    cz.muni.ics: debug
    org.hyperledger.aries: info

# Connection to OIDC interface for authentication into application
oidc:
  issuer: https://login.example.com/oidc/
  registrationId: aai
  client-id: cid
  client-secret: secret
  scopes: ["openid", "profile", "email", "eduperson_entitlement"] # NOTE: profile has to be set at least to fetch userinfo
  redirect_uri: {baseUrl}/login/oauth2/code/{registrationId}
  username_attribute: sub
  admin_entitlements: ["sample_entitlement"]
  admin_subs: ["user@uni.com"]

app:
  # Enabled locales locale - use two character code
  enabled_locales: ["en", "fr"]
  # Default locale - use two character code
  default_locale: "en"
  # Files in the directory must be named in a standard Java resource bundle manner. The filename must be "messages".
  # Example files - (EN) messages_en.properties, (CS) messages_cs.properties, (default) messages.properties
  #localization_files_directory: /etc/rp-services-catalogue/locale/
  # Files in the directory must be named according to the templates defined in the resources/templates folder.
  # These templates will be looked at first, and if they are not found, system will use bundled templates instead.
  #template_files_directory: /etc/rp-services-catalogue/templates/
  # Files in the directory must reflect the requested resource paths. All paths in your templates and fragments
  # should use the local files need to be generated as @{/locale/your/resource.example}
  # These templates will be looked at first, and if they are not found, system will use bundled templates instead.
  #static_resources_directory: /etc/rp-services-catalogue/static/
  support_email: "test@example.org"
  favicon_local: false

# Configuration of connection to Perun
perun:
  adapter:
    # Ext source name via which users are looked up with the combination of user ID from authentication
    ext-source-name: https://idp2.ics.muni.cz/
  connector:
    # RPC conenction details
    rpc:
      url: https://perun-api.example.com/ba/rpc/
      username: username
      password: password
      serializer: json

# Configuration of the Aries agent
aries-agent:
  # websocket connection
#  websocket:
#    url: wss://sample.agent.com/
#    api-key: "test_key"
  # HTTP connection
  http:
    url: https://sample.agent.com/
    api-key: "test_key"
  # Label displayed in the invitations in the wallet
  invitation-label: "FR CESNET Issuer"
```

The application requires to have database in place. Schema initialization file is available in the `resources/sql/` directory.

### Planned extensions

* Startup workers to initialize application with objects like Schemas, Attr definitions etc.
* Enable loading of existing schemas and credential defs from ledger
* Extend administration interface with possibilities to use existing objects as templates for new definitions
* Extend administration interface with ability to define Schemas and similar in a more time efficient form
* Implement deactivation of connection
* Implement revocation of credentials
* Rework the presentations to provide integration points with external systems
