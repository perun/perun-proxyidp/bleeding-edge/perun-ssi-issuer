package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Validated
public class UserExtSource {

    public static final String BEAN_NAME = "UserExtSource";

    @NotNull
    private Long id;

    private ExtSource extSource;

    @NotBlank
    private String login;

    private Timestamp lastAccess;

}
