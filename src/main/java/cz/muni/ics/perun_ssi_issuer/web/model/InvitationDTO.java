package cz.muni.ics.perun_ssi_issuer.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class InvitationDTO {

    @NotNull
    private Long id;

    @NotBlank
    private String alias;

    @NotBlank
    private String invitationUrl;

    @NotBlank
    private String qrCodeData;

    private boolean pending;

}
