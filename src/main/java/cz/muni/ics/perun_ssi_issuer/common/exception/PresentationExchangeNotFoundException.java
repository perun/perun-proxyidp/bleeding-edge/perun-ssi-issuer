package cz.muni.ics.perun_ssi_issuer.common.exception;

public class PresentationExchangeNotFoundException extends Exception {

    public PresentationExchangeNotFoundException() {
        super();
    }

    public PresentationExchangeNotFoundException(String message) {
        super(message);
    }

    public PresentationExchangeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PresentationExchangeNotFoundException(Throwable cause) {
        super(cause);
    }

    protected PresentationExchangeNotFoundException(String message, Throwable cause, boolean enableSuppression,
                                                    boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
