package cz.muni.ics.perun_ssi_issuer.web.controller.admin;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.SchemaAttributeDefinitionService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaAttributeDefinitionForm;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_SCHEMA_ATTRIBUTE_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_CREATE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DELETE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_EDIT;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.SCH_ATTR_DEF_ID;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;

@Controller
@Slf4j
public class SchemaAttributeDefinitionsController extends AppController {

    public static final String MODEL_ATTR_SCHEMA_ATTR_DEF_FORM = "form";

    public static final String MODEL_ATTR_SCHEMA_ATTR_DEF_ID = "schAttrDefId";

    public static final String MODEL_ATTR_SCHEMA_ATTR_DEFINITIONS = "definitions";

    public static final String MODEL_ATTR_SCHEMA_ATTR_DEFINITION = "definition";

    public static final String MODEL_ATTR_DELETE_FAILED = "deleteFailed";

    private final SchemaAttributeDefinitionService service;

    @Autowired
    public SchemaAttributeDefinitionsController(
            @NonNull ApplicationProperties applicationProperties,
            @NonNull OidcProperties oidcProperties,
            SchemaAttributeDefinitionService schemaAttributeDefinitionService) {
        super(applicationProperties, oidcProperties);
        this.service = schemaAttributeDefinitionService;
    }


    @GetMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS)
    public String definitions(Model model) {
        List<SchemaAttributeDefinitionDTO> schemaAttrDefinitions = service.getDefinitions();
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEFINITIONS, schemaAttrDefinitions);
        return VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS;
    }

    @GetMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL)
    public String definition(@PathVariable(SCH_ATTR_DEF_ID) Long schAttrDefId,
                             Model model) throws SchemaAttributeDefinitionNotFoundException {
        SchemaAttributeDefinitionDTO schemaAttrDefinition = service.getDefinition(schAttrDefId);
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEFINITION, schemaAttrDefinition);
        return VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_CREATE)
    public String createDefinition(@ModelAttribute(MODEL_ATTR_SCHEMA_ATTR_DEF_FORM)
                                   SchemaAttributeDefinitionForm form,
                                   Model model) {
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEF_ID, null);
        return VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_CREATE)
    public String createDefinitionSubmit(@Valid @ModelAttribute(MODEL_ATTR_SCHEMA_ATTR_DEF_FORM)
                                         SchemaAttributeDefinitionForm form,
                                         BindingResult bindingResult,
                                         Model model) {
        if (bindingResult.hasErrors()) {
            return VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
        }
        SchemaAttributeDefinitionDTO schemaAttrDefinition = service.createDefinition(form);
        model.addAttribute(SCH_ATTR_DEF_ID, schemaAttrDefinition.getId());
        return "redirect:" + PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
    }

    @GetMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_EDIT)
    public String editDefinition(@PathVariable(SCH_ATTR_DEF_ID) Long schAttrDefId,
                                 Model model)
            throws SchemaAttributeDefinitionNotFoundException {
        SchemaAttributeDefinitionForm form = service.getDefinitionForEdit(schAttrDefId);
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEF_ID, schAttrDefId);
        model.addAttribute(MODEL_ATTR_SCHEMA_ATTR_DEF_FORM, form);
        return VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
    }

    @PostMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_EDIT)
    public String editDefinitionSubmit(@PathVariable(SCH_ATTR_DEF_ID) Long schAttrDefId,
                                       @Valid @ModelAttribute(MODEL_ATTR_SCHEMA_ATTR_DEF_FORM)
                                       SchemaAttributeDefinitionForm form,
                                       BindingResult bindingResult,
                                       Model model)
            throws SchemaAttributeDefinitionNotFoundException {
        if (bindingResult.hasErrors()) {
            return VIEW_SCHEMA_ATTRIBUTE_DEFINITIONS_FORM;
        }
        SchemaAttributeDefinitionDTO schemaAttrDefinition = service.updateDefinition(schAttrDefId, form);
        model.addAttribute(SCH_ATTR_DEF_ID, schemaAttrDefinition.getId());
        return "redirect:" + PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
    }

    @PostMapping(PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DELETE)
    public String deleteDefinition(@PathVariable(SCH_ATTR_DEF_ID) Long schAttrDefId,
                                   Model model)
            throws SchemaAttributeDefinitionNotFoundException {
        boolean success = service.deleteDefinition(schAttrDefId);
        if (!success) {
            model.addAttribute(SCH_ATTR_DEF_ID, schAttrDefId);
            model.addAttribute(MODEL_ATTR_DELETE_FAILED, true);
            return "redirect:" + PATH_SCHEMA_ATTRIBUTE_DEFINITIONS_DETAIL;
        } else {
            return "redirect:" + PATH_SCHEMA_ATTRIBUTE_DEFINITIONS;
        }
    }

}
