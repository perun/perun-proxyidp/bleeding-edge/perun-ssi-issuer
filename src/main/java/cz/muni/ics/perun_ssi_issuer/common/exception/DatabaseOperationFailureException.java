package cz.muni.ics.perun_ssi_issuer.common.exception;

public class DatabaseOperationFailureException extends Exception {

    public DatabaseOperationFailureException() {
        super();
    }

    public DatabaseOperationFailureException(String message) {
        super(message);
    }

    public DatabaseOperationFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseOperationFailureException(Throwable cause) {
        super(cause);
    }

    protected DatabaseOperationFailureException(String message, Throwable cause,
                                                boolean enableSuppression,
                                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
