package cz.muni.ics.perun_ssi_issuer.data.aries;

import cz.muni.ics.perun_ssi_issuer.data.aries.properties.AriesAgentProperties;
import org.hyperledger.aries.AriesClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
public class AriesConfiguration {

    private final AriesAgentProperties agentProperties;

    @Autowired
    public AriesConfiguration(AriesAgentProperties agentProperties) {
        this.agentProperties = agentProperties;
    }

//    @Bean
//    public AriesClient ariesWebSocketClient() {
//        return AriesWebSocketClient
//            .builder()
//            .url(url) // optional - defaults to ws://localhost:8031/ws
//            .apiKey(apiKey) // optional - admin api key if set
//            .handler(new EventHandler.DefaultEventHandler()) // optional - your handler implementation
//            // .bearerToken(bearer) // optional - jwt token - only when running in multi tenant mode
//            .build();
//    }

    @Bean(destroyMethod = "")
    public AriesClient ariesClient() {
        AriesAgentProperties.AriesClientProperties props = agentProperties.getHttp();
        AriesClient.AriesClientBuilder builder = AriesClient.builder();
        builder = builder.url(props.getUrl());
        if (StringUtils.hasText(props.getApiKey())) {
            builder = builder.apiKey(props.getApiKey());
        }
        return builder.build();
    }

}
