package cz.muni.ics.perun_ssi_issuer.middleware.facade.model;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class ConnectionCredentials {

    @NotNull
    private List<CredentialEntity> active;

    @NotNull
    private List<CredentialEntity> revoked;

    @NotNull
    private List<CredentialEntity> unprocessed;

}
