package cz.muni.ics.perun_ssi_issuer.web.controller.admin;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_ADMIN;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_ADMIN;

@Controller
public class AdminController extends AppController {

    @Autowired
    public AdminController(@NonNull ApplicationProperties applicationProperties,
                           @NonNull OidcProperties oidcProperties) {
        super(applicationProperties, oidcProperties);
    }

    @PreAuthorize("hasRole(T(cz.muni.ics.perun_ssi_issuer.web.configuration.SecurityConfiguration).ADMIN)")
    @RequestMapping(PATH_ADMIN)
    public String admin() {
        return VIEW_ADMIN;
    }
}
