package cz.muni.ics.perun_ssi_issuer.web.controller.general;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialConnectionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialCredentialDefinitionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.InvitationNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedInvitationAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.exceptions.AccessForbiddenException;
import cz.muni.ics.perun_ssi_issuer.web.exceptions.BadRequestParameterException;
import cz.muni.ics.perun_ssi_issuer.web.exceptions.NotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.security.auth.login.CredentialNotFoundException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_BAD_REQUEST;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_ERROR;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_FORBIDDEN;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_LOGIN;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_NOT_FOUND;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_UNAUTHORIZED;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_BAD_REQUEST;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_FORBIDDEN;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_GENERAL_ERROR;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_NOT_FOUND;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_UNAUTHORIZED;

@Controller
public class ErrorPagesController extends AppController implements ErrorController {

    // === CONSTRUCTORS === //

    @Autowired
    public ErrorPagesController(@NonNull ApplicationProperties applicationProperties,
                                @NonNull OidcProperties oidcProperties) {
        super(applicationProperties, oidcProperties);
    }

    // === PUBLIC METHODS === //

    @RequestMapping(PATH_ERROR)
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return VIEW_NOT_FOUND;
            } else if (statusCode == HttpStatus.UNAUTHORIZED.value()) {
                return "redirect:" + PATH_LOGIN;
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return VIEW_FORBIDDEN;
            } else if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                return VIEW_BAD_REQUEST;
            }
        }
        return VIEW_GENERAL_ERROR;
    }

    // 400
    @RequestMapping(PATH_BAD_REQUEST)
    public String badRequest() {
        return VIEW_BAD_REQUEST;
    }

    // 401
    @RequestMapping(PATH_UNAUTHORIZED)
    public String unauthorized() {
        return VIEW_UNAUTHORIZED;
    }

    // 403
    @RequestMapping(PATH_FORBIDDEN)
    public String forbidden() {
        return VIEW_FORBIDDEN;
    }

    // 404
    @RequestMapping(PATH_NOT_FOUND)
    public String notFound() {
        return VIEW_NOT_FOUND;
    }

    // === EXCEPTION HANDLERS === //
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BadRequestParameterException.class})
    public String badRequestHandler() {
        return VIEW_BAD_REQUEST;
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({
            BadCredentialsException.class,
    })
    public String unauthorizedHandler() {
        return VIEW_UNAUTHORIZED;
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({
            AccessForbiddenException.class,
            UnauthorizedConnectionAccessException.class,
            UnauthorizedInvitationAccessException.class
    })
    public String forbiddenHandler() {
        return VIEW_FORBIDDEN;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({
            NotFoundException.class,
            UserNotFoundException.class,
            InvitationNotFoundException.class,
            ConnectionNotFoundException.class,
            CredentialDefinitionNotFoundException.class,
            CredentialNotFoundException.class,
            CredentialCredentialDefinitionMismatchException.class,
            CredentialConnectionMismatchException.class
    })
    public String notFoundHandler() {
        return VIEW_NOT_FOUND;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({
            NoUserDetailsAvailableException.class,
            QRCodeGeneratorError.class,
            DatabaseInconsistencyException.class,
            PerunUserNotFoundException.class,
            IllegalArgumentException.class,
            NullPointerException.class,
            Exception.class
    })
    public String internalErrorHandler() {
        return VIEW_GENERAL_ERROR;
    }

}
