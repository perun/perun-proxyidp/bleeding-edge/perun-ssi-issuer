package cz.muni.ics.perun_ssi_issuer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PerunSsiIssuerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PerunSsiIssuerApplication.class, args);
    }

}
