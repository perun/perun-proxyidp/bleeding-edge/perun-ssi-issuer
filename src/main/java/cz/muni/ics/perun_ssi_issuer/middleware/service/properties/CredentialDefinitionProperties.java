package cz.muni.ics.perun_ssi_issuer.middleware.service.properties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class CredentialDefinitionProperties implements InitializingBean {

    private List<CredentialDefinition> definitions;

    private boolean defaultSupportRevocation = false;

    private Integer defaultRevocationRegistrySize = 1000;

    @Override
    public void afterPropertiesSet() {
        if (defaultSupportRevocation)
            if (defaultRevocationRegistrySize < 1) {
                throw new RuntimeException("Invalid configuration - by default, revocation should" +
                        " be supported, but default revocation registry size is misconfigured '"
                        + defaultRevocationRegistrySize + "'");
            }
    }

}
