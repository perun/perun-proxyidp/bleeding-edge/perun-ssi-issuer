package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "User")
@Table(
        name = "users",
        indexes = @Index(name = "idx_users_identifier", columnList = "identifier", unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_users_identifier",
                columnNames = "identifier")
)
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "identifier", nullable = false)
    private String identifier;

    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user")
    @ToString.Exclude
    @Builder.Default
    private List<ConnectionEntity> connections = new ArrayList<>();

    @NotNull
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user")
    @ToString.Exclude
    @Builder.Default
    private List<InvitationEntity> invitations = new ArrayList<>();

    public void addConnection(ConnectionEntity connectionEntity) {
        if (connections == null) {
            connections = new ArrayList<>();
        }
        connections.add(connectionEntity);
        connectionEntity.setUser(this);
    }

    public void addInvitation(InvitationEntity invitationEntity) {
        if (invitations == null) {
            invitations = new ArrayList<>();
        }
        invitations.add(invitationEntity);
        invitationEntity.setUser(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        UserEntity userEntity = (UserEntity) o;
        return id != null && Objects.equals(id, userEntity.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
