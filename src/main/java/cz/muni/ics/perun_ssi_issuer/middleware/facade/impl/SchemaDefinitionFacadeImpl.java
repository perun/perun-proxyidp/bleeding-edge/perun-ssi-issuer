package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.AriesAgentController;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.SchemaAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.SchemaAttributeDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.SchemaDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.SchemaDefinitionFacade;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class SchemaDefinitionFacadeImpl implements SchemaDefinitionFacade {

    private final SchemaDefinitionRepository schemaDefinitionRepository;

    private final SchemaAttributeDefinitionRepository schemaAttributeDefinitionRepository;

    private final AriesAgentController ariesAgentController;

    @Autowired
    public SchemaDefinitionFacadeImpl(@NonNull SchemaDefinitionRepository schemaDefinitionRepository,
                                      @NonNull SchemaAttributeDefinitionRepository schemaAttributeDefinitionRepository,
                                      @NonNull AriesAgentController ariesAgentController) {
        this.schemaDefinitionRepository = schemaDefinitionRepository;
        this.schemaAttributeDefinitionRepository = schemaAttributeDefinitionRepository;
        this.ariesAgentController = ariesAgentController;
    }

    @Override
    public List<SchemaDefinitionEntity> getDefinitions() {
        return schemaDefinitionRepository.findAll();
    }

    @Override
    public SchemaDefinitionEntity getDefinition(@NonNull Long id) throws SchemaDefinitionNotFoundException {
        return schemaDefinitionRepository.findById(id).orElseThrow(SchemaDefinitionNotFoundException::new);
    }

    @Override
    public SchemaDefinitionEntity createDefinition(@NonNull SchemaDefinitionEntity definition,
                                                   @NonNull Set<Long> attributeDefinitions)
            throws SchemaAlreadyExistsException
    {
        Set<SchemaAttributeDefinitionEntity> attributes = new HashSet<>(
                schemaAttributeDefinitionRepository.findAllByIdIn(attributeDefinitions)
        );

        String schemaId = ariesAgentController.createSchema(definition);

        definition.addSchemaAttributeDefinitions(attributes);
        definition.setSchemaDefinitionId(schemaId);

        definition = schemaDefinitionRepository.save(definition);
        schemaAttributeDefinitionRepository.saveAll(attributes);
        return definition;
    }

    @Override
    public void deleteDefinition(@NonNull SchemaDefinitionEntity definition) {
        Set<SchemaAttributeDefinitionEntity> attributes = new HashSet<>(definition.getAttributeDefinitions());
        for (SchemaAttributeDefinitionEntity attribute : attributes) {
            attribute.removeSchemaDefinition(definition);
        }
        schemaAttributeDefinitionRepository.saveAll(attributes);
        schemaDefinitionRepository.delete(definition);
    }

}
