package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CredentialRepository extends JpaRepository<CredentialEntity, Long> {

    List<CredentialEntity> findAllByConnectionAndActiveAndProcessed(
            @NonNull ConnectionEntity connection, boolean active, boolean processed);

    List<CredentialEntity> findAllByConnectionAndProcessed(
            @NonNull ConnectionEntity connection, boolean processed);

    List<CredentialEntity> findAllByCredentialDefinitionAndConnectionAndProcessed(
            @NonNull CredentialDefinitionEntity credentialDefinition,
            @NonNull ConnectionEntity connection,
            boolean processed);

    List<CredentialEntity> findAllByCredentialDefinitionAndConnectionAndProcessedAndActive(
            @NonNull CredentialDefinitionEntity credentialDefinition,
            @NonNull ConnectionEntity connection,
            boolean processed,
            boolean active);

    Optional<CredentialEntity> findByCredentialId(@NonNull String credentialId);

    Optional<CredentialEntity> findByCredentialExchangeId(@NonNull String credentialExchangeId);

    List<CredentialEntity> findAllByCredentialDefinition(@NonNull CredentialDefinitionEntity credentialDefinition);

}
