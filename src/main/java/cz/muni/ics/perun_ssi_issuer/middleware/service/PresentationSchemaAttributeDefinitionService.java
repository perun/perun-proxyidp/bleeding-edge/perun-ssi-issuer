package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationSchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentationSchemaAttributeDefinitionForm;
import lombok.NonNull;

import javax.transaction.Transactional;
import java.util.List;

public interface PresentationSchemaAttributeDefinitionService {

    List<PresentationSchemaAttributeDefinitionDTO> getDefinitions();


    PresentationSchemaAttributeDefinitionDTO getDefinition(@NonNull Long schAttrDefId)
            throws PresentationSchemaAttributeDefinitionNotFoundException;

    PresentationSchemaAttributeDefinitionForm getDefinitionForEdit(@NonNull Long schAttrDefId)
            throws PresentationSchemaAttributeDefinitionNotFoundException;

    @Transactional
    PresentationSchemaAttributeDefinitionDTO createDefinition(@NonNull PresentationSchemaAttributeDefinitionForm form);

    @Transactional
    PresentationSchemaAttributeDefinitionDTO updateDefinition(@NonNull Long schAttrDefId,
                                                  @NonNull PresentationSchemaAttributeDefinitionForm form)
            throws PresentationSchemaAttributeDefinitionNotFoundException;

    @Transactional
    boolean deleteDefinition(@NonNull Long schAttrDefId)
            throws PresentationSchemaAttributeDefinitionNotFoundException;

}
