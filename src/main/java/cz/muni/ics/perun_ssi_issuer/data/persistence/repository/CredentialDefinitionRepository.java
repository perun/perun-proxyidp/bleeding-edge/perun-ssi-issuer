package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface CredentialDefinitionRepository extends
        JpaRepository<CredentialDefinitionEntity, Long> {

    List<CredentialDefinitionEntity> findAllByIdNotIn(@NonNull Collection<Long> ids);

    Optional<CredentialDefinitionEntity> findBySchemaDefinition(
            @NonNull SchemaDefinitionEntity schemaDefinition);

}
