package cz.muni.ics.perun_ssi_issuer.middleware.service.util;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.InvitationEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationExchangeEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.utils.QRCodeGenerator;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.InvitationDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationExchangeDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.UserDetailsDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.references.ConnectionReference;
import cz.muni.ics.perun_ssi_issuer.web.model.references.CredentialDefinitionReference;
import cz.muni.ics.perun_ssi_issuer.web.model.references.PresentationSchemaAttributeDefinitionReference;
import cz.muni.ics.perun_ssi_issuer.web.model.references.PresentationSchemaDefinitionReference;
import cz.muni.ics.perun_ssi_issuer.web.model.references.SchemaAttributeDefinitionReference;
import cz.muni.ics.perun_ssi_issuer.web.model.references.SchemaDefinitionReference;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EntityMappingUtils {

    // === entity MAPPERS === //

    public static UserDetailsDTO map(@NonNull UserEntity entity) {
        return UserDetailsDTO.builder()
                .identifier(entity.getIdentifier())
                .name(entity.getName())
                .email(entity.getEmail())
                .build();
    }

    public static ConnectionDTO map(@NonNull ConnectionEntity entity) {
        return ConnectionDTO.builder()
                .id(entity.getId())
                .alias(entity.getAlias())
                .status(entity.getStatus())
                .build();
    }

    public static InvitationDTO map(@NonNull InvitationEntity entity)
            throws QRCodeGeneratorError {
        return InvitationDTO.builder()
                .id(entity.getId())
                .qrCodeData(QRCodeGenerator.getQRCodeImageBase64(entity.getLink(), 300, 300))
                .alias(entity.getAlias())
                .invitationUrl(entity.getLink())
                .pending(entity.isPending())
                .build();
    }

    public static CredentialDTO map(@NonNull CredentialEntity entity) {
        return CredentialDTO.builder()
                .id(entity.getId())
                .connectionReference(getConnectionReference(entity.getConnection()))
                .attributes(convertEntityAttributes(entity.getAttributes()))
                .accepted(entity.isAccepted())
                .processed(entity.isProcessed())
                .active(entity.isActive())
                .credentialDefinition(map(entity.getCredentialDefinition()))
                .build();
    }

    public static CredentialDefinitionDTO map(@NonNull CredentialDefinitionEntity entity) {
        return CredentialDefinitionDTO.builder()
                .id(entity.getId())
                .schemaDefinition(map(entity.getSchemaDefinition()))
                .build();
    }

    public static SchemaDefinitionDTO map(@NonNull SchemaDefinitionEntity entity) {
        return SchemaDefinitionDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .version(entity.getVersion())
                .attributeDefinitionReferences(
                        getSchemaAttributeDefinitionReferences(entity.getAttributeDefinitions())
                )
                .credentialDefinitionReferences(
                        getCredentialDefinitionReferences(entity.getCredentialDefinitions())
                )
                .build();
    }

    public static SchemaAttributeDefinitionDTO map(@NonNull SchemaAttributeDefinitionEntity entity) {
        return SchemaAttributeDefinitionDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .perunRpcAttribute(entity.getPerunAttribute())
                .mimeType(entity.getMimeType())
                .attributeType(entity.getAttributeType())
                .schemaDefinitionReferences(getSchemaDefinitionReferences(entity.getSchemaDefinitions()))
                .build();
    }

    public static PresentationSchemaDefinitionDTO map(@NonNull PresentationSchemaDefinitionEntity entity) {
        return PresentationSchemaDefinitionDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .schemaId(entity.getSchemaDefinitionId())
                .attributeDefinitionReferences(
                        getPresentationSchemaAttributeDefinitionReferences(entity.getAttributeDefinitions())
                )
                .build();
    }

    public static PresentationSchemaAttributeDefinitionDTO map(@NonNull PresentationSchemaAttributeDefinitionEntity entity) {
        return PresentationSchemaAttributeDefinitionDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .attributeType(entity.getAttributeType())
                .schemaDefinitionReferences(getPresentationSchemaDefinitionReferences(entity.getSchemaDefinitions()))
                .build();
    }

    public static PresentationExchangeDTO map(@NonNull PresentationExchangeEntity presentationExchange) {
        return PresentationExchangeDTO.builder()
                .id(presentationExchange.getId())
                .build();
    }

    // === LIST entity MAPPERS === //

    public static List<ConnectionDTO> mapConnections(List<ConnectionEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<ConnectionDTO> connectionDTOS = new ArrayList<>();
        for (ConnectionEntity entity : entities) {
            connectionDTOS.add(map(entity));
        }
        return connectionDTOS;
    }

    public static List<InvitationDTO> mapInvitations(List<InvitationEntity> entities)
            throws QRCodeGeneratorError {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<InvitationDTO> invitationDTOS = new ArrayList<>();
        for (InvitationEntity entity : entities) {
            invitationDTOS.add(map(entity));
        }
        return invitationDTOS;
    }

    public static List<CredentialDTO> mapCredentials(List<CredentialEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<CredentialDTO> credentialDTOS = new ArrayList<>();
        for (CredentialEntity entity : entities) {
            credentialDTOS.add(map(entity));
        }
        return credentialDTOS;
    }

    public static List<CredentialDefinitionDTO> mapCredentialDefinitions(
            List<CredentialDefinitionEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<CredentialDefinitionDTO> definitions = new ArrayList<>();
        for (CredentialDefinitionEntity entity : entities) {
            definitions.add(map(entity));
        }
        return definitions;
    }

    public static List<SchemaDefinitionDTO> mapSchemaDefinitions(
            List<SchemaDefinitionEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<SchemaDefinitionDTO> definitions = new ArrayList<>();
        for (SchemaDefinitionEntity entity : entities) {
            definitions.add(map(entity));
        }
        return definitions;
    }

    public static List<SchemaAttributeDefinitionDTO> mapSchemaAttributeDefinitions(
            List<SchemaAttributeDefinitionEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<SchemaAttributeDefinitionDTO> definitions = new ArrayList<>();
        for (SchemaAttributeDefinitionEntity entity : entities) {
            definitions.add(map(entity));
        }
        return definitions;
    }

    public static List<PresentationSchemaDefinitionDTO> mapPresentationSchemaDefinitions(
            List<PresentationSchemaDefinitionEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<PresentationSchemaDefinitionDTO> definitions = new ArrayList<>();
        for (PresentationSchemaDefinitionEntity entity : entities) {
            definitions.add(map(entity));
        }
        return definitions;
    }

    public static List<PresentationSchemaAttributeDefinitionDTO> mapPresentationSchemaAttributeDefinitions(
            List<PresentationSchemaAttributeDefinitionEntity> entities) {
        if (entities == null || entities.isEmpty()) {
            return new ArrayList<>();
        }
        List<PresentationSchemaAttributeDefinitionDTO> definitions = new ArrayList<>();
        for (PresentationSchemaAttributeDefinitionEntity entity : entities) {
            definitions.add(map(entity));
        }
        return definitions;
    }

    // === REFERENCES MAPPERS === //

    private static ConnectionReference getConnectionReference(@NonNull ConnectionEntity entity) {
        return ConnectionReference.builder()
                .connectionId(entity.getId())
                .build();
    }

    public static List<CredentialDefinitionReference> getCredentialDefinitionReferences(
            Set<CredentialDefinitionEntity> credentialDefinitions) {
        if (credentialDefinitions == null || credentialDefinitions.isEmpty()) {
            return new ArrayList<>();
        }
        List<CredentialDefinitionReference> references = new ArrayList<>();
        for (CredentialDefinitionEntity entity : credentialDefinitions) {
            references.add(CredentialDefinitionReference.builder()
                    .credentialId(entity.getId())
                    .credentialName(entity.getSchemaDefinition().getName())
                    .build()
            );
        }
        return references;
    }

    public static Set<SchemaAttributeDefinitionReference> getSchemaAttributeDefinitionReferences(
            Set<SchemaAttributeDefinitionEntity> attributeDefinitions) {
        if (attributeDefinitions == null || attributeDefinitions.isEmpty()) {
            return new HashSet<>();
        }
        Set<SchemaAttributeDefinitionReference> references = new HashSet<>();
        for (SchemaAttributeDefinitionEntity entity : attributeDefinitions) {
            references.add(SchemaAttributeDefinitionReference.builder()
                    .attributeId(entity.getId())
                    .attributeName(entity.getName())
                    .build()
            );
        }
        return references;
    }

    public static Set<SchemaDefinitionReference> getSchemaDefinitionReferences(
            Set<SchemaDefinitionEntity> schemaDefinitions) {
        if (schemaDefinitions == null || schemaDefinitions.isEmpty()) {
            return new HashSet<>();
        }
        Set<SchemaDefinitionReference> references = new HashSet<>();
        for (SchemaDefinitionEntity entity : schemaDefinitions) {
            references.add(getSchemaDefinitionReference(entity));
        }
        return references;
    }

    public static Set<PresentationSchemaAttributeDefinitionReference> getPresentationSchemaAttributeDefinitionReferences(
            Set<PresentationSchemaAttributeDefinitionEntity> attributeDefinitions) {
        if (attributeDefinitions == null || attributeDefinitions.isEmpty()) {
            return new HashSet<>();
        }
        Set<PresentationSchemaAttributeDefinitionReference> references = new HashSet<>();
        for (PresentationSchemaAttributeDefinitionEntity entity : attributeDefinitions) {
            references.add(PresentationSchemaAttributeDefinitionReference.builder()
                    .attributeId(entity.getId())
                    .attributeName(entity.getName())
                    .build()
            );
        }
        return references;
    }

    public static Set<PresentationSchemaDefinitionReference> getPresentationSchemaDefinitionReferences(
            Set<PresentationSchemaDefinitionEntity> schemaDefinitions) {
        if (schemaDefinitions == null || schemaDefinitions.isEmpty()) {
            return new HashSet<>();
        }
        Set<PresentationSchemaDefinitionReference> references = new HashSet<>();
        for (PresentationSchemaDefinitionEntity entity : schemaDefinitions) {
            references.add(getPresentationSchemaDefinitionReference(entity));
        }
        return references;
    }

    public static SchemaDefinitionReference getSchemaDefinitionReference(
            @NonNull SchemaDefinitionEntity entity) {
        return SchemaDefinitionReference.builder()
                .schemaId(entity.getId())
                .schemaName(entity.getName())
                .build();
    }

    public static PresentationSchemaDefinitionReference getPresentationSchemaDefinitionReference(
            @NonNull PresentationSchemaDefinitionEntity entity) {
        return PresentationSchemaDefinitionReference.builder()
                .schemaId(entity.getId())
                .schemaName(entity.getName())
                .build();
    }

    // === PRIVATE METHODS === //

    private static Map<String, String> convertEntityAttributes(JsonNode attributes) {
        if (attributes == null || attributes.isNull() || attributes.isEmpty()) {
            return new HashMap<>();
        }
        Map<String, String> mapped = new HashMap<>();
        Iterator<String> it = attributes.fieldNames();
        while (it.hasNext()) {
            String attrName = it.next();
            mapped.put(attrName, attributes.get(attrName).toPrettyString());
        }
        return mapped;
    }

}
