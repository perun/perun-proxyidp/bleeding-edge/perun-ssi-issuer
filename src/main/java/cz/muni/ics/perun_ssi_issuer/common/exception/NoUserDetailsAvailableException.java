package cz.muni.ics.perun_ssi_issuer.common.exception;

public class NoUserDetailsAvailableException extends Exception {

    public NoUserDetailsAvailableException() {
        super();
    }

    public NoUserDetailsAvailableException(String message) {
        super(message);
    }

    public NoUserDetailsAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoUserDetailsAvailableException(Throwable cause) {
        super(cause);
    }

    protected NoUserDetailsAvailableException(String message, Throwable cause, boolean enableSuppression,
                                              boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
