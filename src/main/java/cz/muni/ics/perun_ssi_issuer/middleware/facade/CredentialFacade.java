package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialConnectionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialCredentialDefinitionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import lombok.NonNull;
import org.hyperledger.aries.api.issue_credential_v2.V20CredExRecord;

import java.util.List;

public interface CredentialFacade {

    CredentialEntity getUnprocessedCredential(@NonNull CredentialDefinitionEntity credDef,
                                              @NonNull ConnectionEntity conn)
            throws DatabaseInconsistencyException;

    CredentialEntity getActiveCredential(@NonNull CredentialDefinitionEntity credDef,
                                         @NonNull ConnectionEntity conn)
            throws DatabaseInconsistencyException;

    CredentialEntity issueCredential(@NonNull CredentialDefinitionEntity credDef,
                                     @NonNull ConnectionEntity conn,
                                     @NonNull String userIdentifier)
            throws PerunUserNotFoundException;

    CredentialEntity getCredential(@NonNull ConnectionEntity connection,
                                   @NonNull Long credentialId)
            throws CredentialNotFoundException, CredentialConnectionMismatchException;

    CredentialEntity getCredential(@NonNull CredentialDefinitionEntity credDef,
                                   @NonNull ConnectionEntity conn,
                                   @NonNull Long credentialId)
            throws CredentialConnectionMismatchException, CredentialNotFoundException,
            CredentialCredentialDefinitionMismatchException;

    List<CredentialEntity> getCredentialsByDefinition(@NonNull CredentialDefinitionEntity credDef);

    void handleWebhookCall(@NonNull V20CredExRecord credential);

    CredentialEntity getCredentialCheckStatus(@NonNull CredentialDefinitionEntity credDef,
                                              @NonNull ConnectionEntity conn,
                                              @NonNull Long credentialId)
            throws CredentialConnectionMismatchException, CredentialNotFoundException,
            CredentialCredentialDefinitionMismatchException;
}
