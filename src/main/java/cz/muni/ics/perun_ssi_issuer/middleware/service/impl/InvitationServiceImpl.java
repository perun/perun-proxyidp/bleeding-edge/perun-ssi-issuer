package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.InvitationNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedInvitationAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.InvitationEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.InvitationFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.UserFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.service.InvitationService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.ServiceUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.InvitationDTO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class InvitationServiceImpl implements InvitationService {

    private final UserFacade userFacade;

    private final InvitationFacade invitationFacade;

    @Autowired
    public InvitationServiceImpl(UserFacade userFacade,
                                 InvitationFacade invitationFacade) {
        this.userFacade = userFacade;
        this.invitationFacade = invitationFacade;
    }

    @Override
    public InvitationDTO createInvitation(@NonNull String userAlias,
                                          @NonNull Authentication auth)
            throws UserNotFoundException, QRCodeGeneratorError, NoUserDetailsAvailableException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        UserEntity user = userFacade.getUser(userIdentifier);
        InvitationEntity invitation = invitationFacade.createInvitation(userAlias, user);
        return EntityMappingUtils.map(invitation);
    }

    @Override
    public InvitationDTO getConnectionInvitation(@NonNull Long id,
                                                 @NonNull Authentication auth)
            throws QRCodeGeneratorError, InvitationNotFoundException, NoUserDetailsAvailableException,
            UnauthorizedInvitationAccessException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        InvitationEntity invitationEntity = invitationFacade.getInvitation(id, userIdentifier);
        return EntityMappingUtils.map(invitationEntity);
    }

    @Override
    public List<InvitationDTO> getInvitations(@NonNull Authentication auth)
            throws QRCodeGeneratorError, NoUserDetailsAvailableException
    {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        List<InvitationEntity> invitations = invitationFacade.getPendingInvitations(userIdentifier);
        return EntityMappingUtils.mapInvitations(invitations);
    }

    @Override
    public ConnectionDTO getConnectionForProcessedInvitation(@NonNull Long id,
                                                             @NonNull Authentication auth)
            throws ConnectionNotFoundException, InvitationNotFoundException,
            NoUserDetailsAvailableException, UnauthorizedInvitationAccessException,
            UnauthorizedConnectionAccessException
    {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        ConnectionEntity connection = invitationFacade.getConnectionByInvitation(id, userIdentifier);
        return EntityMappingUtils.map(connection);
    }

    @Override
    public void cancelInvitation(@NonNull Long id,
                                 @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, InvitationNotFoundException,
            UnauthorizedInvitationAccessException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        invitationFacade.cancelInvitation(id, userIdentifier);
    }

    @Override
    public InvitationDTO getConnectionInvitationIfProcessed(@NonNull Long id, @NonNull Authentication auth)
            throws InvitationNotFoundException, NoUserDetailsAvailableException,
            QRCodeGeneratorError, UnauthorizedInvitationAccessException {
        InvitationDTO invitationDTO = getConnectionInvitation(id, auth);
        if (invitationDTO.isPending()) {
            String userIdentifier = ServiceUtils.getUserIdentifier(auth);
            invitationFacade.manuallyCheckInvitation(id, userIdentifier);
            return getConnectionInvitation(id, auth);
        }
        return invitationDTO;
    }

    // === PRIVATE METHODS === //

}
