package cz.muni.ics.perun_ssi_issuer.web.controller;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.web.model.UserDetailsDTO;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

@Slf4j
public abstract class AppController {

    // === CONSTANTS === //

    public static final String AUTHORIZATION_REQUEST_BASE_URI = "/oauth2/authorization";

    // === CONSTANTS - MODEL ATTRS === //

    public static final String MODEL_ATTR_SUPPORT = "support";

    public static final String MODEL_ATTR_LOCALES = "locales";

    public static final String MODEL_ATTR_DEFAULT_LOCALE = "defaultLocale";

    public static final String MODEL_ATTR_FAVICON_PATH = "faviconPath";

    public static final String MODEL_ATTR_LOGIN_LINK = "loginLink";

    // === CLASS FIELDS === //

    @NonNull
    @Getter(AccessLevel.PROTECTED)
    private final ApplicationProperties applicationProperties;

    @NonNull
    private final String clientRegistrationId;

    // === CONSTRUCTORS === //

    public AppController(@NonNull ApplicationProperties applicationProperties,
                         @NonNull OidcProperties oidcProperties) {
        this.applicationProperties = applicationProperties;
        this.clientRegistrationId = oidcProperties.getRegistrationId();
    }

    // === PUBLIC METHODS === //

    @ModelAttribute
    public void commons(@NonNull Model model) {
        model.addAttribute(MODEL_ATTR_SUPPORT, applicationProperties.getSupportEmail());
        model.addAttribute(MODEL_ATTR_LOCALES, applicationProperties.getEnabledLocales());
        model.addAttribute(MODEL_ATTR_DEFAULT_LOCALE, applicationProperties.getDefaultLocale());
        model.addAttribute(MODEL_ATTR_LOGIN_LINK, constructLoginLink());
        String faviconPath = "/img/favicon.ico";
        if (applicationProperties.isFaviconLocal()) {
            faviconPath = "/local" + faviconPath;
        } else {
            faviconPath = "/resources" + faviconPath;
        }
        model.addAttribute(MODEL_ATTR_FAVICON_PATH, faviconPath);
    }

    @ModelAttribute
    public UserDetailsDTO userDetails(Authentication authentication) {
        if (authentication == null) {
            return null;
        }
        OidcUser user = (OidcUser) authentication.getPrincipal();
        return UserDetailsDTO.builder()
                .identifier(user.getSubject())
                .name(user.getFullName())
                .email(user.getEmail())
                .build();
    }

    protected String constructLoginLink() {
        return AUTHORIZATION_REQUEST_BASE_URI + "/" + clientRegistrationId;
    }

}
