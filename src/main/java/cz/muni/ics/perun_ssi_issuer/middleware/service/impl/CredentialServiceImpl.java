package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialConnectionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialCredentialDefinitionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.ConnectionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.service.CredentialService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.ConnCredDefPair;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.ServiceUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.CredentialDTO;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CredentialServiceImpl implements CredentialService {

    private final CredentialFacade credentialFacade;

    private final ConnectionFacade connectionFacade;

    private final CredentialDefinitionFacade credentialDefinitionFacade;

    @Autowired
    public CredentialServiceImpl(CredentialFacade credentialFacade,
                                 ConnectionFacade connectionFacade,
                                 CredentialDefinitionFacade credentialDefinitionFacade) {
        this.credentialFacade = credentialFacade;
        this.connectionFacade = connectionFacade;
        this.credentialDefinitionFacade = credentialDefinitionFacade;
    }

    @Override
    public CredentialDTO issueCredential(@NonNull Long connectionId,
                                         @NonNull Long credentialDefinitionId,
                                         @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, PerunUserNotFoundException {
        ConnCredDefPair connCredDefPair = getConnectionAndCredDefinition(
                connectionId, credentialDefinitionId, auth);
        CredentialDefinitionEntity credDef = connCredDefPair.getCredentialDefinition();
        ConnectionEntity conn = connCredDefPair.getConnection();

        CredentialEntity unprocessedCredential = credentialFacade.getUnprocessedCredential(credDef, conn);
        if (unprocessedCredential != null) {
            return EntityMappingUtils.map(unprocessedCredential);
        }
        CredentialEntity activeCredential = credentialFacade.getActiveCredential(credDef, conn);
        if (activeCredential != null) {
            return EntityMappingUtils.map(activeCredential);
        }
        CredentialEntity credential = credentialFacade.issueCredential(
                credDef, conn, ServiceUtils.getUserIdentifier(auth));

        return EntityMappingUtils.map(credential);
    }

    @Override
    public CredentialDTO getCredential(@NonNull Long connectionId,
                                       @NonNull Long credentialDefinitionId,
                                       @NonNull Long credentialId,
                                       @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException, CredentialCredentialDefinitionMismatchException,
            CredentialConnectionMismatchException, CredentialNotFoundException {
        ConnCredDefPair connCredDefPair = getConnectionAndCredDefinition(
                connectionId, credentialDefinitionId, auth);
        CredentialEntity credential = credentialFacade.getCredential(
                connCredDefPair.getCredentialDefinition(),
                connCredDefPair.getConnection(),
                credentialId);
        return EntityMappingUtils.map(credential);
    }

    @Override
    public CredentialDTO getCredentialForDetail(@NonNull Long connectionId,
                                                @NonNull Long credentialId,
                                                @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, ConnectionNotFoundException,
            DatabaseInconsistencyException, UnauthorizedConnectionAccessException,
            CredentialConnectionMismatchException, CredentialNotFoundException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        ConnectionEntity connection = connectionFacade.getConnection(connectionId, userIdentifier);
        CredentialEntity credential = credentialFacade.getCredential(connection, credentialId);
        return EntityMappingUtils.map(credential);
    }

    @Override
    public List<CredentialDTO> getCredentialsByDefinitionId(@NonNull Long credentialDefinition)
            throws CredentialDefinitionNotFoundException {
        CredentialDefinitionEntity definitionEntity = credentialDefinitionFacade.getCredentialDefinition(credentialDefinition);
        List<CredentialEntity> credentials = credentialFacade.getCredentialsByDefinition(definitionEntity);
        return EntityMappingUtils.mapCredentials(credentials);
    }

    @Override
    public CredentialDTO getCredentialCheckStatus(@NonNull Long connectionId,
                                                  @NonNull Long credentialDefinitionId,
                                                  @NonNull Long credentialId,
                                                  @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException, NoUserDetailsAvailableException,
            DatabaseInconsistencyException, UnauthorizedConnectionAccessException,
            CredentialCredentialDefinitionMismatchException, CredentialConnectionMismatchException,
            CredentialNotFoundException
    {
        ConnCredDefPair connCredDefPair = getConnectionAndCredDefinition(
                connectionId, credentialDefinitionId, auth);
        CredentialEntity credential = credentialFacade.getCredentialCheckStatus(
                connCredDefPair.getCredentialDefinition(),
                connCredDefPair.getConnection(),
                credentialId);
        return EntityMappingUtils.map(credential);
    }

    // === PRIVATE METHODS === //

    private ConnCredDefPair getConnectionAndCredDefinition(@NonNull Long connectionId,
                                                           @NonNull Long credentialDefinitionId,
                                                           @NonNull Authentication auth)
            throws ConnectionNotFoundException, CredentialDefinitionNotFoundException,
            NoUserDetailsAvailableException, DatabaseInconsistencyException,
            UnauthorizedConnectionAccessException {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        ConnectionEntity connectionEntity = connectionFacade
                .getConnection(connectionId, userIdentifier);
        CredentialDefinitionEntity credentialDefinitionEntity = credentialDefinitionFacade
                .getCredentialDefinition(credentialDefinitionId);
        return new ConnCredDefPair(connectionEntity, credentialDefinitionEntity);
    }

}
