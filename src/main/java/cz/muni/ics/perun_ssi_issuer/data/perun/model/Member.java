package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import cz.muni.ics.perun_ssi_issuer.data.perun.enums.MemberStatus;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Validated
public class Member {

    public static final String BEAN_NAME = "Member";

    @NotNull
    private Long id;

    @NotNull
    private Long userId;

    @NotNull
    private Long voId;

    @NotNull
    private MemberStatus status;

}
