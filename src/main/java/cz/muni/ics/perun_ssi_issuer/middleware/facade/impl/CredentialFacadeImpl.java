package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialConnectionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialCredentialDefinitionMismatchException;
import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.data.aries.AriesAgentController;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaAttributeDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.ConnectionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.CredentialRepository;
import cz.muni.ics.perun_ssi_issuer.data.perun.adapter.PerunAdapter;
import cz.muni.ics.perun_ssi_issuer.data.perun.exceptions.PerunUserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.User;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialFacade;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.acy_py.generated.model.V20CredExRecord.StateEnum;
import org.hyperledger.acy_py.generated.model.V20CredExRecordDetail;
import org.hyperledger.aries.api.issue_credential_v2.V20CredExRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hyperledger.acy_py.generated.model.V20CredExRecord.StateEnum.ABANDONED;
import static org.hyperledger.acy_py.generated.model.V20CredExRecord.StateEnum.CREDENTIAL_RECEIVED;
import static org.hyperledger.acy_py.generated.model.V20CredExRecord.StateEnum.DONE;

@Slf4j
@Component
public class CredentialFacadeImpl implements CredentialFacade {
    private final ConnectionRepository connectionRepository;

    private final CredentialRepository credentialRepository;

    private final AriesAgentController agentController;

    private final PerunAdapter perunAdapter;

    @Autowired
    public CredentialFacadeImpl(CredentialRepository credentialRepository,
                                AriesAgentController agentController,
                                PerunAdapter perunAdapter,
                                ConnectionRepository connectionRepository) {
        this.credentialRepository = credentialRepository;
        this.agentController = agentController;
        this.perunAdapter = perunAdapter;
        this.connectionRepository = connectionRepository;
    }

    @Override
    public CredentialEntity getUnprocessedCredential(@NonNull CredentialDefinitionEntity credDef,
                                                     @NonNull ConnectionEntity conn)
            throws DatabaseInconsistencyException {
        List<CredentialEntity> unprocessedCredentials = credentialRepository
                .findAllByCredentialDefinitionAndConnectionAndProcessed(credDef, conn, false);

        if (unprocessedCredentials != null && !unprocessedCredentials.isEmpty()) {
            if (unprocessedCredentials.size() != 1) {
                log.error("DB ERROR - found more than one unprocessed credential for connection " +
                        "'{}' and credential definition '{}'", credDef, conn);
                throw new DatabaseInconsistencyException();
            } else {
                return unprocessedCredentials.get(0);
            }
        }
        return null;
    }

    @Override
    public CredentialEntity getActiveCredential(@NonNull CredentialDefinitionEntity credDef,
                                                @NonNull ConnectionEntity conn)
            throws DatabaseInconsistencyException {
        List<CredentialEntity> activeProcessedCredentials = credentialRepository
                .findAllByCredentialDefinitionAndConnectionAndProcessedAndActive(
                        credDef, conn, true, true);
        if (activeProcessedCredentials != null && !activeProcessedCredentials.isEmpty()) {
            if (activeProcessedCredentials.size() > 1) {
                log.error("DB ERROR - found more than one active credential for connection " +
                        "'{}' and credential definition '{}'", credDef, conn);
                throw new DatabaseInconsistencyException();
            } else {
                return activeProcessedCredentials.get(0);
            }
        }
        return null;
    }

    @Override
    public CredentialEntity issueCredential(@NonNull CredentialDefinitionEntity credDef,
                                            @NonNull ConnectionEntity conn,
                                            @NonNull String userIdentifier)
            throws PerunUserNotFoundException
    {
        SchemaDefinitionEntity schemaDefinition = credDef.getSchemaDefinition();
        JsonNode attributes = fetchAttributesFromPerun(schemaDefinition, userIdentifier);
        V20CredExRecord exchange = agentController.sendCredential(
                credDef.getCredentialDefinitionId(),
                conn.getConnectionId(),
                schemaDefinition.getSchemaDefinitionId(),
                schemaDefinition.getName(),
                attributes
        );
        CredentialEntity credential = CredentialEntity.builder()
                .active(false)
                .processed(false)
                .accepted(false)
                .connection(conn)
                .attributes(attributes)
                .credentialDefinition(credDef)
                .credentialId(exchange.getCredOffer().getId())
                .credentialExchangeId(exchange.getCredentialExchangeId())
                .updatedAt(LocalDateTime.parse(exchange.getUpdatedAt(), DateTimeFormatter.ISO_ZONED_DATE_TIME))
                .build();
        conn.addCredential(credential);
        connectionRepository.save(conn);
        return credentialRepository.findByCredentialExchangeId(exchange.getCredentialExchangeId()).orElseThrow();
    }

    @Override
    public CredentialEntity getCredential(@NonNull ConnectionEntity connection,
                                          @NonNull Long credentialId)
            throws CredentialNotFoundException, CredentialConnectionMismatchException
    {
        CredentialEntity credential = credentialRepository.findById(credentialId).orElse(null);
        if (credential == null) {
            log.error("Credential with ID '{}' not found", credentialId);
            throw new CredentialNotFoundException();
        } else if (!connection.equals(credential.getConnection())) {
            throw new CredentialConnectionMismatchException();
        }
        return credential;
    }

    @Override
    public CredentialEntity getCredential(@NonNull CredentialDefinitionEntity credDef,
                                          @NonNull ConnectionEntity conn,
                                          @NonNull Long credentialId)
            throws CredentialConnectionMismatchException, CredentialNotFoundException,
            CredentialCredentialDefinitionMismatchException
    {
        CredentialEntity credential = getCredential(conn, credentialId);
        if (!credDef.equals(credential.getCredentialDefinition())) {
            throw new CredentialCredentialDefinitionMismatchException();
        }
        return credential;
    }

    @Override
    public List<CredentialEntity> getCredentialsByDefinition(@NonNull CredentialDefinitionEntity credDef) {
        return credentialRepository.findAllByCredentialDefinition(credDef);
    }

    @Override
    public void handleWebhookCall(V20CredExRecord credentialExchange) {
        String credentialExchangeId = credentialExchange.getCredentialExchangeId();
        CredentialEntity entity = credentialRepository
                .findByCredentialExchangeId(credentialExchangeId).orElse(null);
        if (entity == null) {
            log.warn("Received webhook call for non-existing credential with credential exchange ID '{}'",
                    credentialExchangeId);
            return;
        }
        handleExchangeUpdate(entity);
    }

    @Override
    public CredentialEntity getCredentialCheckStatus(@NonNull CredentialDefinitionEntity credDef,
                                                     @NonNull ConnectionEntity conn,
                                                     @NonNull Long credentialId)
            throws CredentialConnectionMismatchException, CredentialNotFoundException,
            CredentialCredentialDefinitionMismatchException
    {
        CredentialEntity entity = getCredential(credDef, conn, credentialId);
        handleExchangeUpdate(entity);
        return entity;
    }

    // === PRIVATE METHODS === //

    private void handleExchangeUpdate(CredentialEntity entity) {
        V20CredExRecordDetail exchange = agentController.getCredentialExchange(entity.getCredentialExchangeId());
        entity.setUpdatedAt(
                LocalDateTime.parse(
                        exchange.getCredExRecord().getUpdatedAt(), DateTimeFormatter.ISO_ZONED_DATE_TIME
                )
        );
        StateEnum state = exchange.getCredExRecord().getState();
        if (state == CREDENTIAL_RECEIVED || state == DONE) {
            entity.setAccepted(true);
            entity.setProcessed(true);
            entity.setActive(true);
            credentialRepository.save(entity);
            entity.setCredentialId(exchange.getIndy().getCredIdStored());
        } else if (state == ABANDONED) {
            entity.setAccepted(false);
            entity.setProcessed(true);
            entity.setActive(false);
            credentialRepository.save(entity);
        }
    }

    private JsonNode fetchAttributesFromPerun(@NonNull SchemaDefinitionEntity schemaDefinition,
                                              @NonNull String userIdentifier)
            throws PerunUserNotFoundException
    {
        User user = perunAdapter.identifyPerunUser(userIdentifier);
        Set<String> perunAttrNames = new HashSet<>();
        for (SchemaAttributeDefinitionEntity attributeDefinition : schemaDefinition.getAttributeDefinitions()) {
            perunAttrNames.add(attributeDefinition.getPerunAttribute());
        }
        Map<String, String> attributeNamesMap = getAttributeNamesMap(schemaDefinition);
        Map<String, JsonNode> attributes = perunAdapter.getUserAttributes(user, perunAttrNames);
        ObjectNode resultJson = JsonNodeFactory.instance.objectNode();
        for (Map.Entry<String, JsonNode> entry : attributes.entrySet()) {
            resultJson.set(attributeNamesMap.get(entry.getKey()), entry.getValue());
        }
        return resultJson;
    }

    private Map<String, String> getAttributeNamesMap(SchemaDefinitionEntity schemaDefinition) {
        Map<String, String> map = new HashMap<>();
        for (SchemaAttributeDefinitionEntity def: schemaDefinition.getAttributeDefinitions()) {
            map.put(def.getPerunAttribute(), def.getName());
        }
        return map;
    }

}
