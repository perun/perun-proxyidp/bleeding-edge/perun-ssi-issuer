package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.enums.ConnectionStatus;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.InvitationNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedInvitationAccessException;
import cz.muni.ics.perun_ssi_issuer.data.aries.AriesAgentController;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.InvitationEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.ConnectionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.InvitationRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.UserRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.InvitationFacade;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.aries.api.connection.ConnectionRecord;
import org.hyperledger.aries.api.connection.CreateInvitationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class InvitationFacadeImpl implements InvitationFacade {

    private final AriesAgentController agentController;

    private final InvitationRepository invitationRepository;

    private final UserRepository userRepository;

    private final ConnectionRepository connectionRepository;

    @Autowired
    public InvitationFacadeImpl(AriesAgentController agentController,
                                InvitationRepository invitationRepository,
                                UserRepository userRepository,
                                ConnectionRepository connectionRepository) {
        this.agentController = agentController;
        this.invitationRepository = invitationRepository;
        this.userRepository = userRepository;
        this.connectionRepository = connectionRepository;
    }

    @Override
    public InvitationEntity createInvitation(String userAlias, UserEntity user) {
        CreateInvitationResponse invitation = agentController.createInvitation();
        InvitationEntity invitationEntity = InvitationEntity.builder()
                .link(invitation.getInvitationUrl())
                .connectionId(invitation.getConnectionId())
                .alias(userAlias)
                .pending(true)
                .user(user)
                .build();

        invitationRepository.save(invitationEntity);
        user.addInvitation(invitationEntity);
        userRepository.save(user);
        return invitationEntity;
    }

    @Override
    public InvitationEntity getInvitation(@NonNull Long id,
                                          @NonNull String userIdentifier)
            throws InvitationNotFoundException, UnauthorizedInvitationAccessException {
        InvitationEntity invitationEntity = invitationRepository.findById(id).orElse(null);
        if (invitationEntity == null) {
            log.warn("Invitation with ID '{}' not found", id);
            throw new InvitationNotFoundException();
        } else if (!userIdentifier.equals(invitationEntity.getUser().getIdentifier())) {
            log.error("User with identifier '{}' tried to load foreign invitation '{}'",
                    userIdentifier, invitationEntity);
            throw new UnauthorizedInvitationAccessException();
        }
        return invitationEntity;
    }

    @Override
    public List<InvitationEntity> getPendingInvitations(@NonNull String userIdentifier) {
        return invitationRepository.findAllByUserIdentifierAndPending(userIdentifier, true);
    }

    @Override
    public ConnectionEntity getConnectionByInvitation(@NonNull Long id,
                                                      @NonNull String userIdentifier)
            throws ConnectionNotFoundException, InvitationNotFoundException,
            UnauthorizedConnectionAccessException, UnauthorizedInvitationAccessException {
        InvitationEntity invitationEntity = getInvitation(id, userIdentifier);
        ConnectionEntity connectionEntity = connectionRepository.findByConnectionId(
                invitationEntity.getConnectionId()).orElse(null);
        if (connectionEntity == null) {
            log.warn("Connection with connection_id '{}' not found",
                    invitationEntity.getConnectionId());
            throw new ConnectionNotFoundException();
        } else if (!userIdentifier.equals(connectionEntity.getUser().getIdentifier())) {
            log.error("User with identifier '{}' tried to load foreign connection '{}'",
                    userIdentifier, connectionEntity);
            throw new UnauthorizedConnectionAccessException();
        }
        return connectionEntity;
    }

    @Override
    public void cancelInvitation(@NonNull Long id, @NonNull String userIdentifier)
            throws InvitationNotFoundException, UnauthorizedInvitationAccessException {
        InvitationEntity invitationEntity = getInvitation(id, userIdentifier);
        if (invitationEntity == null) {
            log.warn("Invitation with invitation_id '{}' not found", id);
            throw new InvitationNotFoundException();
        }
        invitationEntity.setPending(false);
        invitationRepository.save(invitationEntity);
    }

    @Override
    public void manuallyCheckInvitation(@NonNull Long id, @NonNull String userIdentifier)
            throws InvitationNotFoundException, UnauthorizedInvitationAccessException
    {
        InvitationEntity invitationEntity = getInvitation(id, userIdentifier);
        ConnectionRecord record = agentController.getConnection(invitationEntity.getConnectionId());
        processInvitation(invitationEntity, record);
    }

    @Override
    public void handleWebhook(@NonNull ConnectionRecord connectionRecord) {
        String connectionId = connectionRecord.getConnectionId();
        InvitationEntity invitation = invitationRepository
                .findByConnectionId(connectionId).orElse(null);
        if (invitation == null) {
            log.warn("Received webhook call for non-existing invitation with connection_id '{}'",
                    connectionId);
            return;
        }
        processInvitation(invitation, connectionRecord);
    }

    private void processInvitation(InvitationEntity invitationEntity, ConnectionRecord record) {
        if (!record.stateIsActive() && !record.stateIsCompleted()) {
            return;
        }
        ConnectionEntity connectionEntity = connectionRepository.save(ConnectionEntity.builder()
                .connectionId(invitationEntity.getConnectionId())
                .status(ConnectionStatus.ACTIVE)
                .credentials(new ArrayList<>())
                .user(invitationEntity.getUser())
                .alias(invitationEntity.getAlias())
                .updatedAt(LocalDateTime.now())
                .build());

        UserEntity userEntity = connectionEntity.getUser();
        userEntity.addConnection(connectionEntity);
        userRepository.save(userEntity);

        invitationEntity.setPending(false);
        invitationRepository.save(invitationEntity);
    }

}
