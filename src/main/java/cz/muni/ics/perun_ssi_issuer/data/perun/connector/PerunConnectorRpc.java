package cz.muni.ics.perun_ssi_issuer.data.perun.connector;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.common.exception.MissingAttributeMappingException;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.Attribute;
import cz.muni.ics.perun_ssi_issuer.data.perun.model.User;
import cz.muni.ics.perun_ssi_issuer.data.perun.rpc.PerunEntityMapper;
import cz.muni.ics.perun_ssi_issuer.data.perun.rpc.PerunRpcImpl;
import cz.muni.ics.perun_ssi_issuer.data.perun.util.PerunAttributeNameMapper;
import cz.muni.ics.perun_ssi_issuer.data.perun.util.PerunAttributeNameMapping;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class PerunConnectorRpc implements PerunConnector {

    // === MANAGERS === //
    public static final String ATTRIBUTES_MANAGER = "attributesManager";

    public static final String USERS_MANAGER = "usersManager";

    // === METHODS === //
    public static final String GET_USER_BY_EXT_SOURCE_NAME_AND_EXT_LOGIN =
            "getUserByExtSourceNameAndExtLogin";

    public static final String GET_ATTRIBUTE = "getAttribute";

    public static final String GET_ATTRIBUTES = "getAttributes";

    // === PARAMETER NAMES === //
    public static final String EXT_LOGIN = "extLogin";

    public static final String EXT_SOURCE_NAME = "extSourceName";

    public static final String ATTR_NAMES = "attrNames";

    public static final String ATTRIBUTE_NAME = "attributeName";

    // === CLASS FIELDS === //
    @NonNull
    private final PerunRpcImpl rpc;

    @NonNull
    private final PerunAttributeNameMapper perunAttributeNameMapper;

    @Autowired
    public PerunConnectorRpc(@NonNull PerunRpcImpl rpc,
                             @NonNull PerunAttributeNameMapper perunAttributeNameMapper) {
        this.rpc = rpc;
        this.perunAttributeNameMapper = perunAttributeNameMapper;
    }

    @Override
    public String getInternalNameByRpcName(@NonNull String rpcName) {
        if (!StringUtils.hasText(rpcName)) {
            throw new IllegalArgumentException("RpcName cannot be empty");
        }
        PerunAttributeNameMapping mapping = perunAttributeNameMapper.resolveMappingByRpcName(rpcName);
        if (mapping != null) {
            return mapping.getInternalName();
        }
        throw new MissingAttributeMappingException("No AttributeMapping available for RPC name '"
                + rpcName + "'");
    }

    @Override
    public User getUserByExtSourceNameAndExtLogin(@NonNull String extSourceName,
                                                  @NonNull String extLogin) {
        if (!StringUtils.hasText(extSourceName)) {
            throw new IllegalArgumentException("ExtSourceName cannot be empty");
        } else if (!StringUtils.hasText(extLogin)) {
            throw new IllegalArgumentException("ExtLogin cannot be empty");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put(EXT_SOURCE_NAME, extSourceName);
        map.put(EXT_LOGIN, extLogin);

        JsonNode response = rpc.call(USERS_MANAGER, GET_USER_BY_EXT_SOURCE_NAME_AND_EXT_LOGIN, map);
        return PerunEntityMapper.mapUser(response);
    }

    @Override
    public List<Attribute> getAttributes(@NonNull User user, Collection<String> attrNames) {
        return getAttributes("user", user.getId(), attrNames);
    }

    private Attribute getAttribute(@NonNull String entity,
                                   @NonNull Long entityId,
                                   @NonNull String attributeName) {
        if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("AttributeName cannot be empty");
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(entity, entityId);
        map.put(ATTRIBUTE_NAME, attributeName);

        JsonNode response = rpc.call(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);
        return PerunEntityMapper.mapAttribute(response);
    }

    private Attribute getAttribute(@NonNull String entity1,
                                   @NonNull Long entityId1,
                                   @NonNull String entity2,
                                   @NonNull Long entityId2,
                                   @NonNull String attributeName) {
        if (!StringUtils.hasText(attributeName)) {
            throw new IllegalArgumentException("AttributeName cannot be empty");
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(entity1, entityId1);
        map.put(entity2, entityId2);
        map.put(ATTRIBUTE_NAME, attributeName);

        JsonNode response = rpc.call(ATTRIBUTES_MANAGER, GET_ATTRIBUTE, map);
        return PerunEntityMapper.mapAttribute(response);
    }

    private List<Attribute> getAttributes(@NonNull String entity,
                                          @NonNull Long entityId,
                                          @NonNull Collection<String> attrNames) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(entity, entityId);
        map.put(ATTR_NAMES, attrNames);

        JsonNode response = rpc.call(ATTRIBUTES_MANAGER, GET_ATTRIBUTES, map);
        return PerunEntityMapper.mapAttributes(response);
    }

}
