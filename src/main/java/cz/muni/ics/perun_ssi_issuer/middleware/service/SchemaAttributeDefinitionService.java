package cz.muni.ics.perun_ssi_issuer.middleware.service;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaAttributeDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.web.model.SchemaAttributeDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.SchemaAttributeDefinitionForm;
import lombok.NonNull;

import javax.transaction.Transactional;
import java.util.List;

public interface SchemaAttributeDefinitionService {

    List<SchemaAttributeDefinitionDTO> getDefinitions();


    SchemaAttributeDefinitionDTO getDefinition(@NonNull Long schAttrDefId)
            throws SchemaAttributeDefinitionNotFoundException;

    SchemaAttributeDefinitionForm getDefinitionForEdit(@NonNull Long schAttrDefId)
            throws SchemaAttributeDefinitionNotFoundException;

    @Transactional
    SchemaAttributeDefinitionDTO createDefinition(@NonNull SchemaAttributeDefinitionForm form);

    @Transactional
    SchemaAttributeDefinitionDTO updateDefinition(@NonNull Long schAttrDefId,
                                                  @NonNull SchemaAttributeDefinitionForm form)
            throws SchemaAttributeDefinitionNotFoundException;

    @Transactional
    boolean deleteDefinition(@NonNull Long schAttrDefId)
            throws SchemaAttributeDefinitionNotFoundException;

}
