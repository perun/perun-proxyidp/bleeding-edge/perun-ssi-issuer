package cz.muni.ics.perun_ssi_issuer.middleware.service.properties;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class SchemaDefinition {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotBlank
    private String version;

    @NotEmpty
    private List<String> attributeNames;

}
