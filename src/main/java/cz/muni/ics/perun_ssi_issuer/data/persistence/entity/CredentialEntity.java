package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.data.persistence.converter.JsonNodeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "Credential")
@Table(
        name = "credentials",
        indexes = @Index(name = "idx_credentials_credential_id", columnList = "credential_id",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_credentials_credential_id",
                columnNames = "credential_id")
)
public class CredentialEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "credential_id", nullable = false)
    private String credentialId;

    @NotNull
    @Column(name = "cred_exch_id", nullable = false)
    private String credentialExchangeId;

    @NotNull
    @Convert(converter = JsonNodeConverter.class)
    @Column(name = "attributes", nullable = false)
    private JsonNode attributes;

    @Column(name = "processed", nullable = false)
    private boolean processed;

    @Column(name = "accepted", nullable = false)
    private boolean accepted;

    @Column(name = "active", nullable = false)
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "connection_id", nullable = false)
    @ToString.Exclude
    private ConnectionEntity connection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "credential_definition_id", nullable = false)
    @ToString.Exclude
    private CredentialDefinitionEntity credentialDefinition;

    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updatedAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        CredentialEntity that = (CredentialEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
