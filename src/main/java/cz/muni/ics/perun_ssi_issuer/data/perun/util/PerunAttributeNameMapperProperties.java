package cz.muni.ics.perun_ssi_issuer.data.perun.util;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

@Getter
@ToString
@Slf4j

@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "perun.attribute-name-mapper")
public class PerunAttributeNameMapperProperties {

    @NotEmpty
    private final Map<String, PerunAttributeNameMapping> mappings = new HashMap<>();

    public PerunAttributeNameMapperProperties(@NotEmpty Map<String, PerunAttributeNameMapping> mappings) {
        this.mappings.putAll(mappings);
    }

    @PostConstruct
    public void init() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
