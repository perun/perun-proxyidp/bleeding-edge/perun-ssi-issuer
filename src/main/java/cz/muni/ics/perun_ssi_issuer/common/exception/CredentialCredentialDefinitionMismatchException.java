package cz.muni.ics.perun_ssi_issuer.common.exception;

public class CredentialCredentialDefinitionMismatchException extends Exception {

    public CredentialCredentialDefinitionMismatchException() {
        super();
    }

    public CredentialCredentialDefinitionMismatchException(String message) {
        super(message);
    }

    public CredentialCredentialDefinitionMismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public CredentialCredentialDefinitionMismatchException(Throwable cause) {
        super(cause);
    }

    protected CredentialCredentialDefinitionMismatchException(String message, Throwable cause, boolean enableSuppression,
                                                              boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
