package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import com.fasterxml.jackson.databind.JsonNode;
import cz.muni.ics.perun_ssi_issuer.data.persistence.converter.JsonNodeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "PresentationExchangeContent")
@Table(
        name = "presentation_exchange_contents",
        indexes = @Index(name = "idx_pres_exch", columnList = "presentation_exch_id",
                unique = true),
        uniqueConstraints = @UniqueConstraint(name = "constr_unique_pres_exch_id",
                columnNames = "presentation_exch_id")
)
public class PresentationExchangeContentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "presentation_exch_id", nullable = false)
    private PresentationExchangeEntity presentationExchange;

    @NotNull
    @Lob
    @Convert(converter = JsonNodeConverter.class)
    @Column(name = "content", nullable = false)
    private JsonNode content;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        PresentationExchangeContentEntity that = (PresentationExchangeContentEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}

