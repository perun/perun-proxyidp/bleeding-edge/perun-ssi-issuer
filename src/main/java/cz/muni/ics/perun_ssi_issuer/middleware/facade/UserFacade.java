package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserUpdateException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import lombok.NonNull;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

public interface UserFacade {

    UserEntity getUser(@NonNull String userIdentifier) throws UserNotFoundException;

    UserEntity saveOrUpdate(@NonNull OidcUser user, @NonNull String userIdentifier)
            throws UserUpdateException;

}
