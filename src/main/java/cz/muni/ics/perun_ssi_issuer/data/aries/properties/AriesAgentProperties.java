package cz.muni.ics.perun_ssi_issuer.data.aries.properties;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConfigurationException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Slf4j
@ConstructorBinding
@ConfigurationProperties(prefix = "aries-agent", ignoreInvalidFields = true)
public class AriesAgentProperties {

    private final AriesClientProperties websocket;

    private final AriesClientProperties http;

    @NotBlank
    private String invitationLabel;

    @PostConstruct
    public void init() {
        if (websocket == null && http == null) {
            throw new ConfigurationException("No client configuration for Aries provided");
        }
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

    @Getter
    @ToString
    @EqualsAndHashCode
    @AllArgsConstructor
    @ConstructorBinding
    public static class AriesClientProperties {

        private String url;

        private String apiKey;

    }

}
