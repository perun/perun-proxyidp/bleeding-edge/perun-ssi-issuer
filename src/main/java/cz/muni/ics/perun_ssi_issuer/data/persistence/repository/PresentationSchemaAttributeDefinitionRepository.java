package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaAttributeDefinitionEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface PresentationSchemaAttributeDefinitionRepository
        extends JpaRepository<PresentationSchemaAttributeDefinitionEntity, Long>
{

    Collection<PresentationSchemaAttributeDefinitionEntity> findAllByIdIn(@NonNull Collection<Long> ids);

}
