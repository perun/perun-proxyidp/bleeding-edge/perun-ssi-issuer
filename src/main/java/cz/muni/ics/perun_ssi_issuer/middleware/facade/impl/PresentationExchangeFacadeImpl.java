package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationExchangeNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedPresentationExchangeAccessException;
import cz.muni.ics.perun_ssi_issuer.data.aries.AriesAgentController;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationExchangeContentEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationExchangeEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationSchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.ConnectionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.PresentationExchangeContentRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.PresentationExchangeRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.PresentationSchemaDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationExchangeFacade;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.acy_py.generated.model.AttachDecorator;
import org.hyperledger.aries.api.present_proof.PresentationExchangeState;
import org.hyperledger.aries.api.present_proof_v2.V20PresExRecord;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PresentationExchangeFacadeImpl implements PresentationExchangeFacade {

    private final PresentationSchemaDefinitionRepository schemaDefinitionRepository;

    private final ConnectionRepository connectionRepository;

    private final PresentationExchangeRepository presentationExchangeRepository;

    private final PresentationExchangeContentRepository presentationExchangeContentRepository;

    private final AriesAgentController ariesAgentController;

    public PresentationExchangeFacadeImpl(@NonNull PresentationSchemaDefinitionRepository schemaDefinitionRepository,
                                          @NonNull ConnectionRepository connectionRepository,
                                          @NonNull PresentationExchangeRepository presentationExchangeRepository,
                                          @NonNull PresentationExchangeContentRepository presentationExchangeContentRepository,
                                          @NonNull AriesAgentController ariesAgentController)
    {
        this.schemaDefinitionRepository = schemaDefinitionRepository;
        this.connectionRepository = connectionRepository;
        this.presentationExchangeRepository = presentationExchangeRepository;
        this.presentationExchangeContentRepository = presentationExchangeContentRepository;
        this.ariesAgentController = ariesAgentController;
    }

    @Override
    public PresentationExchangeEntity createPresentationExchange(@NonNull Long schemaId,
                                                                 @NonNull Long connectionId,
                                                                 @NonNull String userIdentifier)
            throws ConnectionNotFoundException, UnauthorizedConnectionAccessException, SchemaDefinitionNotFoundException
    {
        ConnectionEntity connection = connectionRepository.findById(
                connectionId).orElse(null);
        if (connection == null) {
            log.warn("Connection with id '{}' not found", connectionId);
            throw new ConnectionNotFoundException();
        } else if (!userIdentifier.equals(connection.getUser().getIdentifier())) {
            log.error("User with identifier '{}' tried to load foreign connection '{}'", userIdentifier, connection);
            throw new UnauthorizedConnectionAccessException();
        }
        PresentationSchemaDefinitionEntity schema = schemaDefinitionRepository.findById(schemaId).orElse(null);
        if (schema == null) {
            log.warn("Schema with id '{}' not found", schemaId);
            throw new SchemaDefinitionNotFoundException();
        }
        V20PresExRecord exchangeRecord = ariesAgentController.createPresentationExchange(schema, connection);
        PresentationExchangeEntity exchangeEntity = PresentationExchangeEntity.builder()
                .connection(connection)
                .presentationExchangeId(exchangeRecord.getPresentationExchangeId())
                .schema(schema)
                .build();
        return presentationExchangeRepository.save(exchangeEntity);
    }

    @Override
    public PresentationExchangeEntity getPresentationExchange(@NonNull Long presExchId,
                                                              @NonNull String userIdentifier)
            throws PresentationExchangeNotFoundException, UnauthorizedPresentationExchangeAccessException,
            DatabaseInconsistencyException
    {
        PresentationExchangeEntity entity = presentationExchangeRepository.findById(presExchId)
                .orElseThrow(PresentationExchangeNotFoundException::new);
        if (entity.getConnection() == null) {
            throw new DatabaseInconsistencyException();
        } else if (entity.getConnection().getUser() == null) {
            throw new DatabaseInconsistencyException();
        } else if (! userIdentifier.equals(entity.getConnection().getUser().getIdentifier())) {
            throw new UnauthorizedPresentationExchangeAccessException();
        }
        return entity;
    }

    @Override
    public void manuallyCheckPresentationExchange(@NonNull Long presExchId, @NonNull String userIdentifier)
            throws UnauthorizedPresentationExchangeAccessException, PresentationExchangeNotFoundException,
            DatabaseInconsistencyException
    {
        PresentationExchangeEntity presentationExchange = getPresentationExchange(presExchId, userIdentifier);
        V20PresExRecord exchangeRecord = ariesAgentController.getPresentationExchange(presentationExchange.getPresentationExchangeId());
        if (exchangeRecord == null) {
            throw new PresentationExchangeNotFoundException("Could not find in Agent");
        }
        processPresentationExchange(presentationExchange, exchangeRecord);
    }

    @Override
    public void handleWebhook(@NonNull V20PresExRecord record) {
        String presExchId = record.getPresentationExchangeId();
        PresentationExchangeEntity presentationExchange = presentationExchangeRepository.findByPresentationExchangeId(presExchId)
                .orElse(null);
        if (presentationExchange == null) {
            log.warn("Received webhook call for non-existing invitation with pres_exch_id '{}'", presExchId);
            return;
        }
        processPresentationExchange(presentationExchange, record);
    }

    private void processPresentationExchange(PresentationExchangeEntity presentationExchange, V20PresExRecord record) {
        if (!record.getState().equals(PresentationExchangeState.PRESENTATION_ACKED)
                && !record.getState().equals(PresentationExchangeState.DONE)
        ) {
            return;
        }
        ArrayNode content = JsonNodeFactory.instance.arrayNode();
        for (AttachDecorator decorator: record.getPres().getPresentationsTildeAttach()) {
            content.add(decorator.getData().getBase64());
        }

        PresentationExchangeContentEntity contentEntity = PresentationExchangeContentEntity.builder()
                        .presentationExchange(presentationExchange)
                        .content(content)
                        .build();

        presentationExchangeContentRepository.save(contentEntity);

        presentationExchange.setCompleted(true);
        presentationExchange.setContent(contentEntity);
        presentationExchangeRepository.save(presentationExchange);
    }

}
