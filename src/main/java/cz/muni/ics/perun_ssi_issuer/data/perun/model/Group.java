package cz.muni.ics.perun_ssi_issuer.data.perun.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Validated
public class Group {

    public static final String BEAN_NAME = "Group";

    @NotNull
    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private Long voId;

    private String uniqueName;

    public Group(@NotNull Long id,
                 @NotNull String name,
                 @NotNull Long voId,
                 String uniqueName) {
        this.setId(id);
        this.setName(name);
        this.setVoId(voId);
        this.setUniqueName(uniqueName);
    }

}
