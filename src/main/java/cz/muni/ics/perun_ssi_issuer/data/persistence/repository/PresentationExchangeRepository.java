package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationExchangeEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PresentationExchangeRepository extends JpaRepository<PresentationExchangeEntity, Long> {

    Optional<PresentationExchangeEntity> findByPresentationExchangeId(@NonNull String presentationExchangeId);

}
