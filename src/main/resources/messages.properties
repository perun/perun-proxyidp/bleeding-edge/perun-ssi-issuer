# COMMON
button.go_to_home_page=Return to the main page
errors.email_msg=We apologize for any inconvenience. You can always contact us at <a href="mailto:{0}">{0}</a>.
app.title=Perun AAI SSI Worker

cz.muni.ics.perun_ssi_issuer.common.validation.LocalizationMapConstraint.message=Value cannot be blank

# NAV
nav.title=Perun AAI SSI Worker
nav.link.home=Home
nav.link.present_proof=Verify information
nav.link.login=Login
nav.link.logout=Logout
nav.link.language=Language
nav.link.admin=Admin
nav.locale.en=English
nav.locale.cs=Czech

locale_en=English
locale_fr=French

# PAGE LOGIN
login.header.heading=Log in
login.header.text=To work with this application, you need to be logged in.
login.body.main_msg=Hit the login button below and log in with your AAI account.
login.body.button.login=Login

# PAGE LOGOUT
logout.header.heading=Logout
logout.header.text=You have asked to log out
logout.body.main_msg=Do you want to really log out of the application?
logout.body.button.yes=Yes, log me out
logout.body.button.no=Stay logged in

# PAGE LOGOUT COMPLETE
logout_complete.header.heading=Logout complete
logout_complete.header.text=Logout completed
logout_complete.body.msg=You have been logged out. If you wish to log back in, continue using the button below.
logout_complete.body.button.login_back=Log back in

# PAGE BAD_REQUEST
bad_request.title=Perun AAI SSI Worker - Bad request
bad_request.heading=Oops! That did not work...
bad_request.text=You have made a request that could not be understood.

# PAGE NOT_FOUND
not_found.title=Perun AAI SSI Worker - 404 Not Found
not_found.heading=Oops! We could not find that...
not_found.text=The content you have tried to access does not exist.

# PAGE FORBIDDEN
forbidden.title=Perun AAI SSI Worker - Forbidden
forbidden.heading=Oops, protected page...
forbidden.text=The content you have tried to access can be displayed only by authorized users. If you are logged in, you probably do not have the privileges to display the content.

# PAGE LOGIN FAILURE
login_failure.heading=Oops, login has failed...
login_failure.text=Your attempt to log in has failed. Please try to log in again, or return to the home page.

# PAGE GENERAL ERROR
general_error.heading=Oops! Something went wrong...
general_error.text=We apologize for any inconvenience. Email us about the error at

# PAGE CONNECTIONS
connection.header.heading=Manage your connections
connection.header.text=To be able to communicate with your wallet you need to have a secure communication cahnnel established. Here you can manage your connections. 
connection.body.connection.heading=Active connections
connection.body.connection.alias=Alias
connection.body.connection.button.details=Details
connection.body.pending_invitation.heading=Pending connections
connection.body.pending_invitation.alias=Connection alias:
connection.body.pending_invitation.button.details=Details
connection.body.pending_invitation.no_invitation=There are no pending invitations
connection.body.new_connection.text=Do not have connection yet, or want to create a new one? Continue with the button below!
connection.body.new_connection.button.create_connection=Create new connection

# PAGE INVITATION_CREATE
invitation_create.header.heading=Create connection
invitation_create.header.text=We need to create a secure connection with your wallet application. Continue by steps below
invitation_create.body.form.alias.error_validation=Alias cannot be empty
invitation_create.body.form.alias.error_used=Alias "{0}" is already used
invitation_create.body.form.qr_generating_error=Error when generating QR code. Do you want to try again?
invitation_create.body.form.alias.label=Alias for this connection
invitation_create.body.form.alias.placeholder=Wallet on Mi10
invitation_create.body.form.alias.hint=This will help you identify the device/wallet for the connection
invitation_create.body.form.button.submit=Create connection

# PAGE INVITATION_CHECK_STATUS
invitation_check_status.header.heading=Invitation
invitation_check_status.header.text=We are now ready to establish a secure communication channel. Continue with the steps described below.
invitation_check_status.body.qrcode.information_text=a) Scan the QR Code below with your mobile wallet:
invitation_check_status.body.qrcode.img_alt=QR Code
invitation_check_status.body.link.information_text=b) copy the link below to your wallet app if you cannot scan the QR code
invitation_check_status.body.form.text=Now click one of the buttons below to check the status or cancel the invitation.
invitation_check_status.body.form.button.submit=I have done it, check the connection status
invitation_check_status.body.form.button.cancel=Close this invitation (I cannot complete it)

# PAGE INVITATION_PROCESSED
invitation_processed.header.heading=Invitation processed
invitation_processed.header.text=
invitation_processed.body.success=Congratulations, a secure connection is now created. How would you like to proceed?
invitation_processed.body.button.connection=Issue credential for this connection
invitation_processed.body.button.overview=Go back to overview

# PAGE CONNECTION_DETAIL
connection_detail.header.heading=Connection details
connection_detail.header.text=Below you can find details for the chosen connection. Particularly, you can use this page to manage credentials associated with this connection.
connection_detail.body.button.deactivate=Deactivate (not working)
connection_detail.body.unprocessed_credentials.heading=Unprocessed credentials
connection_detail.body.unprocessed_credentials.attributes_heading=Attributes
connection_detail.body.unprocessed_credentials.no_credentials=No credentials to display
connection_detail.body.unprocessed_credentials.button.detail=Detail
connection_detail.body.active_credentials.heading=Active credentials
connection_detail.body.active_credentials.attributes_heading=Attributes
connection_detail.body.active_credentials.no_credentials=No credentials to display
connection_detail.body.active_credentials.button.detail=Detail
connection_detail.body.revoked_credentials.heading=Revoked credentials
connection_detail.body.revoked_credentials.attributes_heading=Attributes
connection_detail.body.revoked_credentials.button.detail=Detail
connection_detail.body.revoked_credentials.no_credentials=No credentials to display
connection_detail.body.available_credentials.heading=Available credential
connection_detail.body.available_credentials.no_credentials=No credentials to display
connection_detail.body.available_credentials.button.issue=Get it here

# PAGE CREDENTIAL_ISSUE
credential_issue.header.heading=Issue credential
credential_issue.header.text=You have started the process of issuing a chosen credential. Proceed with the steps described below.
credential_issue.body.form.text=You should now receive a prompt to accept the credential in your wallet.
credential_issue.body.form.button.submit=Check status

# PAGE CREDENTIAL_PROCESSED
credential_processed.header.heading=Credential processed
credential_processed.header.text=
credential_processed.body.message_success=Credential is now available in your wallet
credential_processed.body.message_failure=We are sorry to hear you have not accepted the credential
credential_processed.body.button.connection=Go back to connection
credential_processed.body.button.issue_again=Issue again
credential_processed.body.button.credential=Check detail

# PAGE CREDENTIAL_DETAIL
credential_detail.header.heading=Credential detail
credential_detail.header.text=This page shows detailed credential information
credential_detail.body.button.back_to_connection=Back to connection
credential_detail.body.button.back_to_overview=Back to overview
credential_detail.body.button.revoke=Revoke (not working)

# PAGE ADMIN
admin.header.heading=Administration interface
admin.header.text=
admin.body.issuer.heading=Configuration of Issuer part
admin.body.issuer.button.schema_attribute_defs=Attribute definitions
admin.body.issuer.button.schema_defs=Schema definitions
admin.body.issuer.button.cred_defs=Credential definitions
admin.body.verifier.heading=Configuration of Verifier part
admin.body.verifier.button.pres_schema_attribute_defs=Presentation attribute definitions
admin.body.verifier.button.pres_schema_defs=Presentation schema definitions

# PAGE CREDENTIAL DEF DETAIL
credential_def_detail.header.heading=Credential definition detail
credential_def_detail.header.text=This page shows details of the credential definition
credential_def_detail.body.button.overview=Back to definitions
credential_def_detail.body.button.delete=Delete
credential_def_detail.body.table.name=Schema name
credential_def_detail.body.table.description=Schema description
credential_def_detail.body.table.version=Version
credential_def_detail.body.table.credentials=credentials
credential_def_detail.body.table.credentials.no_creds=No credentials of this type have been issued yet
credential_def_detail.body.table.credentials.button.cred_details=Details

# PAGE CREDENTIAL DEF FORM
credential_def_form.header.heading=Credential definition form
credential_def_form.header.text=Fill in/modify the form to create/update the credential definition
credential_def_form.body.button.overview=Back to overview
credential_def_form.body.alert_exists.heading=Credential definition already exists
credential_def_form.body.alert_exists.text=Credential definition for the given schema already exists (id: {0})
credential_def_form.body.alert_schema_not_found.heading=Schema not found
credential_def_form.body.alert_schema_not_found.text=Selected schema has not been found
credential_def_form.body.form.schema_definition.label=Schema definition
credential_def_form.body.form.schema_definition.help=Choose the schema for the credential
credential_def_form.body.form.support_revocation.label=Support revocation
credential_def_form.body.form.support_revocation.help=Mark the checkbox to enable revocable credentials
credential_def_form.body.form.support_revocation.label_yes=Enabled
credential_def_form.body.form.support_revocation.label_no=Disabled
credential_def_form.body.form.revocation_registry_size.label=Revocation registry size
credential_def_form.body.form.revocation_registry_size.help=If you have enabled revocation, set up the size of the registry for revoked credentials
credential_def_form.body.form.button.submit=Submit

# PAGE CREDENTIAL DEFS
credential_defs.header.heading=Credential definitions
credential_defs.header.text=Overview of the created credential definitions
credential_defs.body.button.admin=Admin panel
credential_defs.body.button.create_def=Create definition
credential_defs.body.no_defs=No definitions available
credential_defs.body.button.cred_def_detail=Detail

# PAGE SCHEMA ATTR DEF DETAIL
schema_attr_def_detail.header.heading=Schema attribute definition
schema_attr_def_detail.header.text=Details of the schema definition
schema_attr_def_detail.body.button.overview=Back to overview
schema_attr_def_detail.body.button.edit=Edit definition
schema_attr_def_detail.body.button.delete=Delete definition
schema_attr_def_detail.body.name=Name
schema_attr_def_detail.body.rpc_name=Perun RPC Name
schema_attr_def_detail.body.mime_type=MIME Type
schema_attr_def_detail.body.type=Value type
schema_attr_def_detail.body.schemas=Schemas, where the definition is used
schema_attr_def_detail.body.schemas.no_defs=Used in no definitions

# PAGE SCHEMA ATTR DEF FORM - CREATE
schema_attr_def_form.create.header.heading=Create attribute definition
schema_attr_def_form.create.header.text=Fill in the form below to create a new credential schema attribute
schema_attr_def_form.create.body.button.overview=Back to overview
schema_attr_def_form.create.body.form.name.label=Name
schema_attr_def_form.create.body.form.name.help=System used name for the schema attribute
schema_attr_def_form.create.body.form.rpc_name.label=Perun RPC attribute name
schema_attr_def_form.create.body.form.rpc_name.help=URN of the mapped Perun attribute
schema_attr_def_form.create.body.form.mime_type.label=MIME Type
schema_attr_def_form.create.body.form.mime_type.help=MIME Type of the attribute value
schema_attr_def_form.create.body.form.type.label=Type
schema_attr_def_form.create.body.form.type.help=Type of the attribute value
schema_attr_def_form.create.body.form.type.select.type.STRING=String
schema_attr_def_form.create.body.form.type.select.type.INT=Number
schema_attr_def_form.create.body.form.type.select.type.BOOLEAN=Boolean
schema_attr_def_form.create.body.form.type.select.type.LIST=List<String>
schema_attr_def_form.create.body.form.type.select.type.MAP=Map<String, String>
schema_attr_def_form.create.body.form.name_translations.label=Name translations
schema_attr_def_form.create.body.form.name_translations.help=Translations for name of the attribute (in UI)
schema_attr_def_form.create.body.form.description_translations.label=Description translations
schema_attr_def_form.create.body.form.description_translations.help=Translations for description of the attribute (in UI)
schema_attr_def_form.create.body.form.button.submit=Submit

# PAGE SCHEMA ATTR DEF FORM - UPDATE
schema_attr_def_form.edit.header.heading=Update attribute definition
schema_attr_def_form.edit.header.text=Update the form below to edit existing schema attribute
schema_attr_def_form.edit.body.button.overview=Back to overview
schema_attr_def_form.edit.body.form.name.label=Name
schema_attr_def_form.edit.body.form.name.help=System used name for the schema attribute
schema_attr_def_form.edit.body.form.rpc_name.label=Perun RPC attribute name
schema_attr_def_form.edit.body.form.rpc_name.help=URN of the mapped Perun attribute
schema_attr_def_form.edit.body.form.mime_type.label=MIME Type
schema_attr_def_form.edit.body.form.mime_type.help=MIME Type of the attribute value
schema_attr_def_form.edit.body.form.type.label=Type
schema_attr_def_form.edit.body.form.type.help=Type of the attribute value
schema_attr_def_form.edit.body.form.type.select.type.STRING=String
schema_attr_def_form.edit.body.form.type.select.type.INT=Number
schema_attr_def_form.edit.body.form.type.select.type.BOOLEAN=Boolean
schema_attr_def_form.edit.body.form.type.select.type.LIST=List<String>
schema_attr_def_form.edit.body.form.type.select.type.MAP=Map<String, String>
schema_attr_def_form.edit.body.form.name_translations.label=Name translations
schema_attr_def_form.edit.body.form.name_translations.help=Translations for name of the attribute (in UI)
schema_attr_def_form.edit.body.form.description_translations.label=Description translations
schema_attr_def_form.edit.body.form.description_translations.help=Translations for description of the attribute (in UI)
schema_attr_def_form.edit.body.form.button.submit=Submit

# PAGE SCHEMA ATTR DEFS
schema_attr_defs.header.heading=Schema attribute definitions
schema_attr_defs.header.text=Overview of the created schema attribute definitions
schema_attr_defs.body.button.admin=Admin panel
schema_attr_defs.body.button.create_def=Create definition
schema_attr_defs.body.button.detail=Detail
schema_attr_defs.body.no_defs=No definitions found

# PAGE SCHEMA DEF DETAIL
schema_def_detail.header.heading=Schema definition detail
schema_def_detail.header.text=See the configured details
schema_def_detail.body.button.overview=Back to definitions
schema_def_detail.body.button.edit=Edit definition
schema_def_detail.body.button.delete=Delete definition
schema_def_detail.body.name=Name
schema_def_detail.body.version=Version
schema_def_detail.body.sch_attr_defs=Attribute definitions
schema_def_detail.body.sch_attr_defs.no_defs=No definitions found
schema_def_detail.body.cred_defs=Credential definitions
schema_def_detail.body.cred_defs.no_defs=No definitions found

# PAGE SCHEMA DEF FORM - CREATE
schema_def_form.create.header.heading=Create schema definition
schema_def_form.create.header.text=Fill in the form below
schema_def_form.create.body.button.overview=Back to overview
schema_def_form.create.body.alert_exists.heading=Schema already exists
schema_def_form.create.body.alert_exists.text=Schema with the given name and version already exists (id: {0}).
schema_def_form.create.body.form.name.label=Name
schema_def_form.create.body.form.name.help=System used name
schema_def_form.create.body.form.version.label=Version
schema_def_form.create.body.form.version.help=Version in number format (e.g. 1.0)
schema_def_form.create.body.form.attributes.label=Attributes
schema_def_form.create.body.form.attributes.help=Attributes to be included in the schema
schema_def_form.create.body.form.name_translations.label=Name translations
schema_def_form.create.body.form.name_translations.help=Translations for name of the schema (in UI)
schema_def_form.create.body.form.description_translations.label=Desc translations
schema_def_form.create.body.form.description_translations.help=Translations for description of the schema (in UI)
schema_def_form.create.body.form.button.submit=Submit

# PAGE SCHEMA DEF FORM - UPDATE
schema_def_form.edit.header.heading=Update schema definition
schema_def_form.edit.header.text=Fill in the form below
schema_def_form.edit.body.button.overview=Back to overview
schema_def_form.edit.body.form.name.label=Name
schema_def_form.edit.body.form.name.help=System used name for the schema
schema_def_form.edit.body.form.version.label=Version
schema_def_form.edit.body.form.version.help=Version in number format (e.g. 1.0)
schema_def_form.edit.body.form.attributes.label=Attributes
schema_def_form.edit.body.form.attributes.help=Attributes to be included in the schema
schema_def_form.edit.body.form.name_translations.label=Name translations
schema_def_form.edit.body.form.name_translations.help=Translations for name of the schema (in UI)
schema_def_form.edit.body.form.description_translations.label=Desc translations
schema_def_form.edit.body.form.description_translations.help=Translations for description of the schema (in UI)
schema_def_form.edit.body.form.button.submit=Submit

# PAGE SCHEMA DEFS
schema_defs.header.heading=Schema definitions
schema_defs.header.text=Overview of the created schema definitions
schema_defs.body.button.admin=Admin panel
schema_defs.body.button.create_def=Create definition
schema_defs.body.button.detail=Details
schema_defs.body.no_defs=No definitions found

# PAGE PRES. SCHEMA ATTR DEF DETAIL
pres_schema_attr_def_detail.header.heading=Presentation schema attribute definition
pres_schema_attr_def_detail.header.text=Detail of the schema attribute definition for PRESENTATIONS (proofs)
pres_schema_attr_def_detail.body.button.overview=Back to overview
pres_schema_attr_def_detail.body.button.edit=Edit definition
pres_schema_attr_def_detail.body.button.delete=Delete definition
pres_schema_attr_def_detail.body.name=Name
pres_schema_attr_def_detail.body.type=Value type
pres_schema_attr_def_detail.body.schemas=Schemas, where the definition is used
pres_schema_attr_def_detail.body.schemas.no_defs=Used in no definitions

# PAGE PRES. SCHEMA ATTR DEF FORM - CREATE
pres_schema_attr_def_form.create.header.heading=Create attribute definition
pres_schema_attr_def_form.create.header.text=Heading
pres_schema_attr_def_form.create.body.button.overview=Back to overview
pres_schema_attr_def_form.create.body.form.name.label=Name
pres_schema_attr_def_form.create.body.form.name.help=System used name for the schema attribute
pres_schema_attr_def_form.create.body.form.type.label=Type
pres_schema_attr_def_form.create.body.form.type.help=Type of the value
pres_schema_attr_def_form.create.body.form.type.select.type.STRING=String
pres_schema_attr_def_form.create.body.form.type.select.type.INT=Number
pres_schema_attr_def_form.create.body.form.type.select.type.BOOLEAN=Boolean
pres_schema_attr_def_form.create.body.form.type.select.type.LIST=List<String>
pres_schema_attr_def_form.create.body.form.type.select.type.MAP=Map<String, String>
pres_schema_attr_def_form.create.body.form.name_translations.label=Name translations
pres_schema_attr_def_form.create.body.form.name_translations.help=Translations for name of the schema (in UI)
pres_schema_attr_def_form.create.body.form.description_translations.label=Description translations
pres_schema_attr_def_form.create.body.form.description_translations.help=Translations for description of the schema (in UI)
pres_schema_attr_def_form.create.body.form.button.submit=Submit

# PAGE PRES. SCHEMA ATTR DEF FORM - UPDATE
pres_schema_attr_def_form.edit.header.heading=Update attribute definition
pres_schema_attr_def_form.edit.header.text=Text
pres_schema_attr_def_form.edit.body.button.overview=Back to overview
pres_schema_attr_def_form.edit.body.form.name.label=Name
pres_schema_attr_def_form.edit.body.form.name.help=Name help
pres_schema_attr_def_form.edit.body.form.type.label=Type
pres_schema_attr_def_form.edit.body.form.type.help=Type of the value
pres_schema_attr_def_form.edit.body.form.type.select.type.STRING=String
pres_schema_attr_def_form.edit.body.form.type.select.type.INT=Number
pres_schema_attr_def_form.edit.body.form.type.select.type.BOOLEAN=Boolean
pres_schema_attr_def_form.edit.body.form.type.select.type.LIST=List<String>
pres_schema_attr_def_form.edit.body.form.type.select.type.MAP=Map<String, String>
pres_schema_attr_def_form.edit.body.form.name_translations.label=Translations for name of the schema (in UI)
pres_schema_attr_def_form.edit.body.form.name_translations.help=Name translations help
pres_schema_attr_def_form.edit.body.form.description_translations.label=Description translations
pres_schema_attr_def_form.edit.body.form.description_translations.help=Translations for description of the schema (in UI)
pres_schema_attr_def_form.edit.body.form.button.submit=Submit

# PAGE PRES. SCHEMA ATTR DEFS
pres_schema_attr_defs.header.heading=Presentation schema attribute definitions
pres_schema_attr_defs.header.text=Overview of the created schema attribute definitions
pres_schema_attr_defs.body.button.admin=Admin panel
pres_schema_attr_defs.body.button.create_def=Create definition
pres_schema_attr_defs.body.button.detail=Detail
pres_schema_attr_defs.body.no_defs=No definitions found

# PAGE PRES. SCHEMA DEF DETAIL
pres_schema_def_detail.header.heading=Presentation schema definition detail
pres_schema_def_detail.header.text=See the configured details
pres_schema_def_detail.body.button.overview=Back to definitions
pres_schema_def_detail.body.button.edit=Edit definition
pres_schema_def_detail.body.button.delete=Delete definition
pres_schema_def_detail.body.name=Name
pres_schema_def_detail.body.schema_id=Schema ID
pres_schema_def_detail.body.sch_attr_defs=Attribute definitions
pres_schema_def_detail.body.sch_attr_defs.no_defs=No definitions found

# PAGE PRES. SCHEMA DEF FORM - CREATE
pres_schema_def_form.create.header.heading=Create presentation schema definition
pres_schema_def_form.create.header.text=Fill in the form below
pres_schema_def_form.create.body.button.overview=Back to overview
pres_schema_def_form.create.body.form.name.label=Name
pres_schema_def_form.create.body.form.name.help=Name help
pres_schema_def_form.create.body.form.schemaId.label=Schema
pres_schema_def_form.create.body.form.schemaId.help=Schema ID help
pres_schema_def_form.create.body.form.attributes.label=Attributes
pres_schema_def_form.create.body.form.attributes.help=Attributes help
pres_schema_def_form.create.body.form.name_translations.label=Name translations
pres_schema_def_form.create.body.form.name_translations.help=Translations for name of the schema (in UI)
pres_schema_def_form.create.body.form.description_translations.label=Desc translations
pres_schema_def_form.create.body.form.description_translations.help=Translations for description of the schema (in UI)
pres_schema_def_form.create.body.form.button.submit=Submit

# PAGE PRES. SCHEMA DEF FORM - UPDATE
pres_schema_def_form.edit.header.heading=Update presentation schema definition
pres_schema_def_form.edit.header.text=Fill in the form below
pres_schema_def_form.edit.body.button.overview=Back to overview
pres_schema_def_form.edit.body.form.name.label=Name
pres_schema_def_form.edit.body.form.name.help=System used name
pres_schema_def_form.edit.body.form.schemaId.label=Schema ID
pres_schema_def_form.edit.body.form.schemaId.help=Schema ID - identifier of the schema on the ledger
pres_schema_def_form.edit.body.form.attributes.label=Attributes
pres_schema_def_form.edit.body.form.attributes.help=Attributes to be included in the definition
pres_schema_def_form.edit.body.form.name_translations.label=Name translations
pres_schema_def_form.edit.body.form.name_translations.help=Translations for name of the schema (in UI)
pres_schema_def_form.edit.body.form.description_translations.label=Desc translations
pres_schema_def_form.edit.body.form.description_translations.help=Translations for description of the schema (in UI)
pres_schema_def_form.edit.body.form.button.submit=Submit

# PAGE PRES. SCHEMA DEFS
pres_schema_defs.header.heading=Presentation schema definitions
pres_schema_defs.header.text=Overview of the created presentation schema definitions
pres_schema_defs.body.button.admin=Admin panel
pres_schema_defs.body.button.create_def=Create definition
pres_schema_defs.body.button.detail=Details
pres_schema_defs.body.no_defs=No definitions found

# PAGE PRESENT_PROOF_SELECTION
present_proof_selection.header.heading=Present credential
present_proof_selection.header.text=Present credential to verify the information. Choose the credential type and connection to present the credential
present_proof_selection.body.no_connections=Seems you do not have any connection created. Use the button below to set up one.
present_proof_selection.body.no_connections.button_create_connection=Create connection
present_proof_selection.body.no_schemas=Seems the administrator has not configured any credential to be usable for verification of user information.
present_proof_selection.body.form.connection_label=Select connection
present_proof_selection.body.form.schema_label=Select credential type
present_proof_selection.body.form.button.submit=Start

# PAGE PRESENT_PROOF_PROMPT
present_proof_prompt.header.heading=Present proof
present_proof_prompt.header.text=Present proof to verify information - check the status
present_proof_prompt.body.form.text=Click button below to check if the request has been processed
present_proof_prompt.body.form.button.submit=Check

# PAGE PRESENT_PROOF_PROMPT_SUCCESS
present_proof_prompt_success.header.heading=Success
present_proof_prompt_success.header.text=Successfully processed
present_proof_prompt_success.body.message_success=Congratulations, the credential has been processed.
