package cz.muni.ics.perun_ssi_issuer.web.model.references;

import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.DefinitionLocalization;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class PresentationSchemaDefinitionReference extends DefinitionLocalization {

    private Long schemaId;

    private String schemaName;

    @Override
    public String getName() {
        return schemaName;
    }

    public String i18nNameKey() {
        return TranslationFacade.getPresSchDefI18nNameKey(getName());
    }

    public String i18nDescKey() {
        return TranslationFacade.getPresSchDefI18nDescKey(getName());
    }

}
