package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.TranslationEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TranslationRepository extends JpaRepository<TranslationEntity, Long> {

    TranslationEntity findByMsgKeyAndLocale(@NonNull String msgKey, @NonNull String locale);

    List<TranslationEntity> findAllByMsgKey(@NonNull String name);

    void deleteAllByMsgKey(@NonNull String msgKey);

}
