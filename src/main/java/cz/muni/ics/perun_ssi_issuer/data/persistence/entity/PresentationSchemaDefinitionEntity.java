package cz.muni.ics.perun_ssi_issuer.data.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
//DB
@Entity(name = "PresentationSchemaDefinition")
@Table(
        name = "presentation_schema_definitions",
        indexes = {
                @Index(name = "idx_pres_schema_defs_schema_definition_id", columnList = "schema_definition_id",
                        unique = true),
                @Index(name = "idx_pres_schema_defs_name", columnList = "name",
                        unique = true)
        },
        uniqueConstraints = {
                @UniqueConstraint(name = "constr_unique_pres_schema_defs_schema_definition_id",
                        columnNames = "schema_definition_id"),
                @UniqueConstraint(name = "constr_unique_schema_defs_name",
                        columnNames = "name")
        }
)
public class PresentationSchemaDefinitionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "schema_definition_id", nullable = false)
    private String schemaDefinitionId;

    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @NotEmpty
    @ManyToMany
    @JoinTable(
            name = "pres_schema_definition_pres_schema_attribute_definition",
            joinColumns = @JoinColumn(name = "pres_schema_definition_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "pres_schema_attribute_definition_id", nullable = false)
    )
    @ToString.Exclude
    @Builder.Default
    private Set<PresentationSchemaAttributeDefinitionEntity> attributeDefinitions = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ||
                Hibernate.getClass(this) != Hibernate.getClass(o)) {
            return false;
        }
        PresentationSchemaDefinitionEntity that = (PresentationSchemaDefinitionEntity) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public void addAttributeDefinition(@NonNull PresentationSchemaAttributeDefinitionEntity definition) {
        attributeDefinitions.add(definition);
        definition.addSchemaDefinition(this);
    }

    public void removeAttributeDefinition(@NonNull PresentationSchemaAttributeDefinitionEntity definition) {
        attributeDefinitions.remove(definition);
        definition.removeSchemaDefinition(this);
    }

    public void addAttributeDefinition(@NonNull Collection<PresentationSchemaAttributeDefinitionEntity> attributes) {
        for (PresentationSchemaAttributeDefinitionEntity def : attributes) {
            addAttributeDefinition(def);
        }
    }

    public void removeSchemaAttributeDefinitions(@NonNull Collection<PresentationSchemaAttributeDefinitionEntity> attributes) {
        for (PresentationSchemaAttributeDefinitionEntity def : attributes) {
            removeAttributeDefinition(def);
        }
    }

}

