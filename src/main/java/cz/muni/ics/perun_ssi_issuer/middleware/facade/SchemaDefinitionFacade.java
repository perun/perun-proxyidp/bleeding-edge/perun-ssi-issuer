package cz.muni.ics.perun_ssi_issuer.middleware.facade;

import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.SchemaAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import lombok.NonNull;

import java.util.List;
import java.util.Set;

public interface SchemaDefinitionFacade {

    List<SchemaDefinitionEntity> getDefinitions();

    SchemaDefinitionEntity getDefinition(@NonNull Long id) throws SchemaDefinitionNotFoundException;

    SchemaDefinitionEntity createDefinition(@NonNull SchemaDefinitionEntity definition,
                                            @NonNull Set<Long> attributeDefinitions) throws SchemaAlreadyExistsException;

    void deleteDefinition(@NonNull SchemaDefinitionEntity definition);

}
