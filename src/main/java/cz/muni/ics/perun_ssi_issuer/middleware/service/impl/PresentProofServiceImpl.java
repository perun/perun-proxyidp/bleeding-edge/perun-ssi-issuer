package cz.muni.ics.perun_ssi_issuer.middleware.service.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationExchangeNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedPresentationExchangeAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.ConnectionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.PresentationExchangeEntity;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.ConnectionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationExchangeFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.PresentationSchemaDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentProofService;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.EntityMappingUtils;
import cz.muni.ics.perun_ssi_issuer.middleware.service.util.ServiceUtils;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationExchangeDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentProofForm;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PresentProofServiceImpl implements PresentProofService {

    private final ConnectionFacade connectionFacade;

    private final PresentationExchangeFacade presentationExchangeFacade;

    private final PresentationSchemaDefinitionFacade presentationSchemaDefinitionFacade;

    @Autowired
    public PresentProofServiceImpl(@NonNull ConnectionFacade connectionFacade,
                                   @NonNull PresentationExchangeFacade presentationExchangeFacade,
                                   @NonNull PresentationSchemaDefinitionFacade presentationSchemaDefinitionFacade)
    {
        this.connectionFacade = connectionFacade;
        this.presentationExchangeFacade = presentationExchangeFacade;
        this.presentationSchemaDefinitionFacade = presentationSchemaDefinitionFacade;
    }

    @Override
    public List<ConnectionDTO> getUserConnections(@NonNull Authentication auth)
            throws NoUserDetailsAvailableException, UserNotFoundException
    {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        List<ConnectionEntity> connectionEntities = connectionFacade.getActiveUserConnections(userIdentifier);
        return EntityMappingUtils.mapConnections(connectionEntities);
    }

    @Override
    public List<PresentationSchemaDefinitionDTO> getSchemasForProofs() {
        return EntityMappingUtils.mapPresentationSchemaDefinitions(
                presentationSchemaDefinitionFacade.getDefinitions()
        );
    }

    @Override
    public PresentationExchangeDTO createPresentationExchange(@NonNull PresentProofForm proofSelectionForm,
                                                              @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, ConnectionNotFoundException,
            SchemaDefinitionNotFoundException, UnauthorizedConnectionAccessException
    {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        PresentationExchangeEntity presentationExchange = presentationExchangeFacade.createPresentationExchange(
                proofSelectionForm.getSchemaId(), proofSelectionForm.getConnectionId(), userIdentifier
        );
        return EntityMappingUtils.map(presentationExchange);
    }

    @Override
    public boolean checkPresentationExchangeStatus(@NonNull Long presExchId, @NonNull Authentication auth)
            throws NoUserDetailsAvailableException, UnauthorizedPresentationExchangeAccessException,
            PresentationExchangeNotFoundException, DatabaseInconsistencyException
    {
        String userIdentifier = ServiceUtils.getUserIdentifier(auth);
        PresentationExchangeEntity exchange = presentationExchangeFacade.getPresentationExchange(
                presExchId, userIdentifier
        );
        if (!exchange.isCompleted()) {
            presentationExchangeFacade.manuallyCheckPresentationExchange(presExchId, userIdentifier);
            exchange = presentationExchangeFacade.getPresentationExchange(presExchId, userIdentifier);
        }
        return exchange.isCompleted();
    }

}
