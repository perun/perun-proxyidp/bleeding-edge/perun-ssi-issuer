package cz.muni.ics.perun_ssi_issuer.common;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.TranslationEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.TranslationRepository;
import org.springframework.context.support.AbstractMessageSource;

import java.text.MessageFormat;
import java.util.Locale;

public class DBMessageSource extends AbstractMessageSource {

    private final TranslationRepository languageRepository;

    public DBMessageSource(TranslationRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @Override
    protected MessageFormat resolveCode(String key, Locale locale) {
        TranslationEntity message = languageRepository.findByMsgKeyAndLocale(
                key, locale.getLanguage());
        if (message == null) {
            return null;
        } else {
            return new MessageFormat(message.getContent(), locale);
        }
    }

}
