package cz.muni.ics.perun_ssi_issuer.web;

import cz.muni.ics.perun_ssi_issuer.data.aries.properties.AriesAgentProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;

@Slf4j
public class ApiKeyAuthenticationManager implements AuthenticationManager {

    private final Set<String> apiKeys = new HashSet<>();

    public ApiKeyAuthenticationManager(AriesAgentProperties properties) {
        if (properties.getHttp() != null && StringUtils.hasText(properties.getHttp().getApiKey())) {
            this.apiKeys.add(properties.getHttp().getApiKey());
        }
        if (properties.getWebsocket() != null && StringUtils.hasText(properties.getWebsocket().getApiKey())) {
            this.apiKeys.add(properties.getWebsocket().getApiKey());
        }
        log.info("API Key authentication provider for webhooks configured. Accepting '{}' key(s)" +
                ". (Note: if 0, API key is not checked)", apiKeys.size());
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        String apiKey = (String) authentication.getCredentials();
        if (apiKeys.isEmpty() || isValidApiKey(apiKey)) {
            authentication.setAuthenticated(true);
            return authentication;
        }
        throw new BadCredentialsException("The API key was not found or not the expected value.");
    }

    private boolean isValidApiKey(String apiKey) {
        return StringUtils.hasText(apiKey) && apiKeys.contains(apiKey);
    }

}
