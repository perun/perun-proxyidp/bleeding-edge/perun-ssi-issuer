package cz.muni.ics.perun_ssi_issuer.web;

import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserIdentifierAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserUpdateException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.UserService;
import cz.muni.ics.perun_ssi_issuer.web.model.UserDetailsDTO;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class RecordUpdatingSavedRequestAwareAuthnSuccessHandler extends
        SavedRequestAwareAuthenticationSuccessHandler {
    @Setter
    @Autowired
    private UserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication)
            throws ServletException, IOException {
        UserDetailsDTO userDetailsDTO;
        try {
            userDetailsDTO = userService.saveOrUpdateUserDetails(authentication);
        } catch (NoUserIdentifierAvailableException
                 | NoUserDetailsAvailableException
                 | UserUpdateException e) {
            throw new ServletException(e);
        }
        log.debug("Processed user details: '{}'", userDetailsDTO);
        super.onAuthenticationSuccess(request, response, authentication);
    }

}
