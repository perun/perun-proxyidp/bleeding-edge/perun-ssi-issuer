package cz.muni.ics.perun_ssi_issuer.web.controller.user;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.DatabaseInconsistencyException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.PresentationExchangeNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedPresentationExchangeAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.PresentProofService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationExchangeDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.PresentationSchemaDefinitionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.PresentProofForm;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRESENT_PROOF;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRESENT_PROOF_EXCHANGE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_PRESENT_PROOF_EXCHANGE_SUCCESS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_SUCCESS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PRES_EXCH_ID;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRESENT_PROOF_PROMPT;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRESENT_PROOF_PROMPT_SUCCESS;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_PRESENT_PROOF_SELECTION;

@Controller
@Slf4j
public class VerifierController extends AppController {

    // === CONSTANTS - MODEL ATTRS === //

    public static final String MODEL_ATTR_PROOF_SELECTION_FORM = "proofSelectionForm";

    public static final String MODEL_ATTR_CONNECTIONS = "connections";

    public static final String MODEL_ATTR_CREDENTIAL_SCHEMAS = "credentialSchemas";

    public static final String MODEL_ATTR_PRES_EXCH_ID = PRES_EXCH_ID;

    // === CLASS FIELDS === //

    private final PresentProofService presentProofService;

    // === CONSTRUCTORS === //

    @Autowired
    public VerifierController(@NonNull ApplicationProperties applicationProperties,
                              @NonNull OidcProperties oidcProperties,
                              @NonNull PresentProofService presentProofService)
    {
        super(applicationProperties, oidcProperties);
        this.presentProofService = presentProofService;
    }

    @GetMapping(path = PATH_PRESENT_PROOF)
    public String presentProofSelect(@ModelAttribute(MODEL_ATTR_PROOF_SELECTION_FORM)
                                     PresentProofForm proofSelectionForm,
                                     Model model,
                                     Authentication auth)
            throws UserNotFoundException, NoUserDetailsAvailableException
    {
        List<ConnectionDTO> connections = presentProofService.getUserConnections(auth);
        List<PresentationSchemaDefinitionDTO> credentialSchemas = presentProofService.getSchemasForProofs();
        model.addAttribute(MODEL_ATTR_CONNECTIONS, connections);
        model.addAttribute(MODEL_ATTR_CREDENTIAL_SCHEMAS, credentialSchemas);
        return VIEW_PRESENT_PROOF_SELECTION;
    }

    @PostMapping(path = PATH_PRESENT_PROOF)
    public String presentProofSelectSubmit(@NonNull @ModelAttribute(MODEL_ATTR_PROOF_SELECTION_FORM)
                                           PresentProofForm proofSelectionForm,
                                           Authentication auth)
            throws ConnectionNotFoundException, SchemaDefinitionNotFoundException,
            NoUserDetailsAvailableException, UnauthorizedConnectionAccessException
    {
        PresentationExchangeDTO presExchId = presentProofService.createPresentationExchange(proofSelectionForm, auth);
        return "redirect:" + PATH_PRESENT_PROOF + '/' + presExchId.getId();
    }

    @RequestMapping(path = PATH_PRESENT_PROOF_EXCHANGE, method = {RequestMethod.GET, RequestMethod.POST})
    public String presentProofPrompt(@PathVariable(PRES_EXCH_ID) Long presExchId,
                                     Model model,
                                     Authentication auth)
            throws NoUserDetailsAvailableException, UnauthorizedPresentationExchangeAccessException,
            PresentationExchangeNotFoundException, DatabaseInconsistencyException
    {
        boolean success = presentProofService.checkPresentationExchangeStatus(presExchId, auth);
        model.addAttribute(MODEL_ATTR_PRES_EXCH_ID, presExchId);
        if (success) {
            return "redirect:" + PATH_PRESENT_PROOF + '/' + presExchId + PATH_SUCCESS;
        } else {
            return VIEW_PRESENT_PROOF_PROMPT;
        }
    }

    @GetMapping(path = PATH_PRESENT_PROOF_EXCHANGE_SUCCESS)
    public String presentProofPrompt(@PathVariable(PRES_EXCH_ID) Long presExchId)
    {
        return VIEW_PRESENT_PROOF_PROMPT_SUCCESS;
    }

    // === PRIVATE METHODS === //

}
