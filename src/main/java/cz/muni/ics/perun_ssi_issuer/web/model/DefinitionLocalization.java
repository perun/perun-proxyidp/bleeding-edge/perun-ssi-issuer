package cz.muni.ics.perun_ssi_issuer.web.model;

public abstract class DefinitionLocalization {

    public abstract String getName();

    public abstract String i18nNameKey();

    public abstract String i18nDescKey();

}
