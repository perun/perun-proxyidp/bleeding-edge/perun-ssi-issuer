package cz.muni.ics.perun_ssi_issuer.middleware.service.properties;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Slf4j
@AllArgsConstructor

@Validated
@ConstructorBinding
@ConfigurationProperties(prefix = "object-definitions")
public class ObjectDefinitionProperties implements InitializingBean {

    @NotEmpty
    private final List<SchemaAttributeDefinition> schemaAttributes;

    @NotEmpty
    private final List<SchemaDefinition> schemas;

    @NotNull
    private final CredentialDefinitionProperties credentials;

    @Override
    public void afterPropertiesSet() {
        log.info("Initialized '{}' properties", this.getClass().getSimpleName());
        log.debug("{}", this);
    }

}
