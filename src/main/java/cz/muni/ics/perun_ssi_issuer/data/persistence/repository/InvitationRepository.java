package cz.muni.ics.perun_ssi_issuer.data.persistence.repository;

import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.InvitationEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InvitationRepository extends JpaRepository<InvitationEntity, Long> {

    List<InvitationEntity> findAllByUserIdentifierAndPending(@NonNull String userIdentifier,
                                                             boolean pending);

    Optional<InvitationEntity> findByConnectionId(@NonNull String connectionId);

}
