package cz.muni.ics.perun_ssi_issuer.web.controller.user;

import cz.muni.ics.perun_ssi_issuer.ApplicationProperties;
import cz.muni.ics.perun_ssi_issuer.common.exception.AliasUsedException;
import cz.muni.ics.perun_ssi_issuer.common.exception.ConnectionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.InvitationNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.NoUserDetailsAvailableException;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedConnectionAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UnauthorizedInvitationAccessException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.middleware.service.InvitationService;
import cz.muni.ics.perun_ssi_issuer.web.controller.AppController;
import cz.muni.ics.perun_ssi_issuer.web.model.ConnectionDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.InvitationDTO;
import cz.muni.ics.perun_ssi_issuer.web.model.forms.InvitationForm;
import cz.muni.ics.perun_ssi_issuer.web.properties.OidcProperties;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.INV_ID;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_CONNECTIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_INVITATIONS;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_INVITATIONS_CANCEL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_INVITATIONS_CREATE;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_INVITATIONS_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.PathConstants.PATH_INVITATIONS_PROCESSED;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_INVITATION_CREATE;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_INVITATION_DETAIL;
import static cz.muni.ics.perun_ssi_issuer.web.ViewConstants.VIEW_INVITATION_PROCESSED;

@Controller
@Slf4j
public class InvitationsController extends AppController {

    // === CONSTANTS - MODEL ATTRS === //

    public static final String MODEL_ATTR_INVITATION_FORM = "invitationForm";

    public static final String MODEL_ATTR_ALIAS_USED_ERROR = "aliasUsedError";

    public static final String MODEL_ATTR_QR_GENERATING_ERROR = "qrGeneratingError";

    public static final String MODEL_ATTR_INVITATION = "invitation";

    public static final String MODEL_ATTR_CONNECTION_ID = "connectionId";

    // === CLASS FIELDS === //

    private final InvitationService invitationService;

    // === CONSTRUCTORS === //

    @Autowired
    public InvitationsController(@NonNull ApplicationProperties applicationProperties,
                                 @NonNull OidcProperties oidcProperties,
                                 @NonNull InvitationService invitationService) {
        super(applicationProperties, oidcProperties);
        this.invitationService = invitationService;
    }

    @GetMapping(path = PATH_INVITATIONS_CREATE)
    public String invitationCreate(@ModelAttribute(MODEL_ATTR_INVITATION_FORM)
                                   InvitationForm invitationForm) {
        return VIEW_INVITATION_CREATE;
    }

    @PostMapping(path = PATH_INVITATIONS_CREATE)
    public String invitationCreateSubmit(@Valid @ModelAttribute(MODEL_ATTR_INVITATION_FORM)
                                         InvitationForm invitationForm,
                                         BindingResult bindingResult,
                                         Model model,
                                         Authentication auth)
            throws UserNotFoundException, InvitationNotFoundException, NoUserDetailsAvailableException {
        if (bindingResult.hasErrors()) {
            return VIEW_INVITATION_CREATE;
        }
        String alias = invitationForm.getAlias();
        InvitationDTO invitationDTO;
        try {
            invitationDTO = invitationService.createInvitation(alias, auth);
        } catch (AliasUsedException ex) {
            model.addAttribute(MODEL_ATTR_ALIAS_USED_ERROR, alias);
            return VIEW_INVITATION_CREATE;
        } catch (QRCodeGeneratorError e) {
            model.addAttribute(MODEL_ATTR_QR_GENERATING_ERROR, alias);
            return VIEW_INVITATION_CREATE;
        }

        return "redirect:" + PATH_INVITATIONS + '/' + invitationDTO.getId();
    }

    @GetMapping(path = PATH_INVITATIONS_DETAIL)
    public String invitationDetail(@PathVariable(INV_ID) Long invId,
                                   Model model,
                                   Authentication auth)
            throws QRCodeGeneratorError, InvitationNotFoundException, NoUserDetailsAvailableException,
            UnauthorizedInvitationAccessException {
        InvitationDTO invitationDTO = invitationService.getConnectionInvitation(invId, auth);
        model.addAttribute(MODEL_ATTR_INVITATION, invitationDTO);
        return VIEW_INVITATION_DETAIL;
    }

    @PostMapping(path = PATH_INVITATIONS_DETAIL)
    public String invitationCheckProcessed(@PathVariable(INV_ID) Long invId,
                                           Authentication auth)
            throws QRCodeGeneratorError, InvitationNotFoundException, NoUserDetailsAvailableException,
            UnauthorizedInvitationAccessException {
        InvitationDTO invitationDTO = invitationService.getConnectionInvitationIfProcessed(invId, auth);
        if (invitationDTO.isPending()) {
            return "redirect:" + PATH_INVITATIONS_DETAIL;
        } else {
            return "redirect:" + PATH_INVITATIONS_PROCESSED;
        }
    }

    @PostMapping(path = PATH_INVITATIONS_CANCEL)
    public String invitationCancel(@PathVariable(INV_ID) Long invId,
                                   Authentication auth)
            throws InvitationNotFoundException, NoUserDetailsAvailableException,
            UnauthorizedInvitationAccessException {
        invitationService.cancelInvitation(invId, auth);
        return "redirect:" + PATH_CONNECTIONS;
    }

    @GetMapping(path = PATH_INVITATIONS_PROCESSED)
    public String invitationProcessed(@PathVariable(INV_ID) Long invId,
                                      Model model,
                                      Authentication auth)
            throws ConnectionNotFoundException, InvitationNotFoundException,
            NoUserDetailsAvailableException, UnauthorizedInvitationAccessException,
            UnauthorizedConnectionAccessException {
        ConnectionDTO connection = invitationService.getConnectionForProcessedInvitation(invId, auth);
        model.addAttribute(MODEL_ATTR_CONNECTION_ID, connection.getId());
        return VIEW_INVITATION_PROCESSED;
    }

    // === PRIVATE METHODS === //

}
