package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.CredentialDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.SchemaDefinitionNotFoundException;
import cz.muni.ics.perun_ssi_issuer.data.aries.AriesAgentController;
import cz.muni.ics.perun_ssi_issuer.data.aries.exception.CredDefAlreadyExistsException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.CredentialEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.SchemaDefinitionEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.CredentialDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.SchemaDefinitionRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.CredentialDefinitionFacade;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.model.ConnectionCredentials;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class CredentialDefinitionFacadeImpl implements CredentialDefinitionFacade {

    private final CredentialDefinitionRepository credentialDefinitionRepository;

    private final SchemaDefinitionRepository schemaDefinitionRepository;

    private final AriesAgentController ariesAgentController;

    @Autowired
    public CredentialDefinitionFacadeImpl(
            @NonNull CredentialDefinitionRepository credentialDefinitionRepository,
            @NonNull SchemaDefinitionRepository schemaDefinitionRepository,
            @NonNull AriesAgentController ariesAgentController) {
        this.credentialDefinitionRepository = credentialDefinitionRepository;
        this.schemaDefinitionRepository = schemaDefinitionRepository;
        this.ariesAgentController = ariesAgentController;
    }

    @Override
    public List<CredentialDefinitionEntity> getAvailableCredentials(
            @NonNull ConnectionCredentials credentials) {
        Set<Long> unavailableDefinitionsIds = new HashSet<>();
        for (CredentialEntity active : credentials.getActive()) {
            unavailableDefinitionsIds.add(active.getCredentialDefinition().getId());
        }
        for (CredentialEntity inProcessing : credentials.getUnprocessed()) {
            unavailableDefinitionsIds.add(inProcessing.getCredentialDefinition().getId());
        }

        List<CredentialDefinitionEntity> availableCredentials;
        if (unavailableDefinitionsIds.isEmpty()) {
            availableCredentials = credentialDefinitionRepository.findAll();
        } else {
            availableCredentials =
                    credentialDefinitionRepository.findAllByIdNotIn(unavailableDefinitionsIds);
        }
        return availableCredentials;
    }

    @Override
    public CredentialDefinitionEntity getCredentialDefinition(@NonNull Long id)
            throws CredentialDefinitionNotFoundException {
        return credentialDefinitionRepository
                .findById(id)
                .orElseThrow(CredentialDefinitionNotFoundException::new);
    }

    @Override
    public List<CredentialDefinitionEntity> getDefinitions() {
        return credentialDefinitionRepository.findAll();
    }

    @Override
    public CredentialDefinitionEntity createCredentialDefinition(@NonNull Long schemaDefId,
                                                                 boolean supportRevocation,
                                                                 Integer revocationRegistrySize)
            throws SchemaDefinitionNotFoundException, CredDefAlreadyExistsException {
        SchemaDefinitionEntity schemaDefinition = schemaDefinitionRepository.findById(schemaDefId)
                .orElseThrow(SchemaDefinitionNotFoundException::new);
        String credentialDefinitionId = ariesAgentController.createCredentialDefinition(
                schemaDefinition, supportRevocation, revocationRegistrySize
        );
        CredentialDefinitionEntity entity = CredentialDefinitionEntity.builder()
                .credentialDefinitionId(credentialDefinitionId)
                .schemaDefinition(schemaDefinition)
                .supportRevocation(supportRevocation)
                .build();
        schemaDefinition.addCredentialDefinition(entity);
        schemaDefinitionRepository.save(schemaDefinition);
        return credentialDefinitionRepository.findBySchemaDefinition(schemaDefinition).orElseThrow();
    }

    @Override
    public void deleteCredentialDefinition(@NonNull Long id) throws CredentialDefinitionNotFoundException {
        CredentialDefinitionEntity entity = credentialDefinitionRepository.findById(id)
                .orElseThrow(CredentialDefinitionNotFoundException::new);

        SchemaDefinitionEntity schemaDefinition = entity.getSchemaDefinition();
        schemaDefinition.removeCredentialDefinition(entity);

        schemaDefinitionRepository.save(schemaDefinition);
        credentialDefinitionRepository.delete(entity);
    }
}
