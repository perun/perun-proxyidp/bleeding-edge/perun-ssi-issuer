package cz.muni.ics.perun_ssi_issuer.middleware.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import cz.muni.ics.perun_ssi_issuer.common.exception.QRCodeGeneratorError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;


@Slf4j
public class QRCodeGenerator {

    public static String getQRCodeImageBase64(@NotNull String text, int width, int height)
            throws QRCodeGeneratorError {
        if (!StringUtils.hasText(text)) {
            throw new IllegalArgumentException("Text cannot be empty");
        } else if (width <= 0) {
            throw new IllegalArgumentException("Width has to be a positive non-zero number");
        } else if (height <= 0) {
            throw new IllegalArgumentException("Width has to be a positive non-zero number");
        }
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

            ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
            MatrixToImageConfig con = new MatrixToImageConfig(0xFF000002, 0xFFFFC041);

            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream, con);
            byte[] qrCodeBytes = pngOutputStream.toByteArray();
            return Base64.getEncoder().encodeToString(qrCodeBytes);
        } catch (WriterException | IOException ex) {
            log.error("Error when generating code text: '{}', width: '{}', height: '{}'",
                    text, width, height, ex);
            throw new QRCodeGeneratorError(ex);
        }
    }

}

