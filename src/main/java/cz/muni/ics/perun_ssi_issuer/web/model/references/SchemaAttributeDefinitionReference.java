package cz.muni.ics.perun_ssi_issuer.web.model.references;

import cz.muni.ics.perun_ssi_issuer.middleware.facade.TranslationFacade;
import cz.muni.ics.perun_ssi_issuer.web.model.DefinitionLocalization;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class SchemaAttributeDefinitionReference extends DefinitionLocalization {

    private Long attributeId;

    private String attributeName;

    @Override
    public String getName() {
        return attributeName;
    }

    @Override
    public String i18nNameKey() {
        return TranslationFacade.getSchAttrDefI18nNameKey(getName());
    }

    @Override
    public String i18nDescKey() {
        return TranslationFacade.getSchAttrDefI18nDescKey(getName());
    }

}
