package cz.muni.ics.perun_ssi_issuer.middleware.facade.impl;

import cz.muni.ics.perun_ssi_issuer.common.exception.UserNotFoundException;
import cz.muni.ics.perun_ssi_issuer.common.exception.UserUpdateException;
import cz.muni.ics.perun_ssi_issuer.data.persistence.entity.UserEntity;
import cz.muni.ics.perun_ssi_issuer.data.persistence.repository.UserRepository;
import cz.muni.ics.perun_ssi_issuer.middleware.facade.UserFacade;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Slf4j
@Component
public class UserFacadeImpl implements UserFacade {

    private final UserRepository userRepository;

    @Autowired
    public UserFacadeImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity getUser(@NonNull String userIdentifier) throws UserNotFoundException {
        UserEntity userEntity = userRepository.findByIdentifier(userIdentifier).orElse(null);
        if (userEntity == null) {
            log.warn("User with identifier '{}' not found", userIdentifier);
            throw new UserNotFoundException();
        }
        return userEntity;
    }

    @Override
    public UserEntity saveOrUpdate(@NonNull OidcUser user, @NonNull String userIdentifier)
            throws UserUpdateException {
        UserEntity userEntity = userRepository.findByIdentifier(userIdentifier).orElse(null);
        if (userEntity != null) {
            log.debug("Found existing user '{}', updating the record", user);
            userEntity.setIdentifier(userIdentifier);
            userEntity.setName(user.getFullName());
            userEntity.setEmail(user.getEmail());
            log.debug("User record before update '{}'", userEntity);
        } else {
            log.debug("No user found for userIdentifier '{}', creating a new record",
                    userIdentifier);
            userEntity = UserEntity.builder()
                    .identifier(userIdentifier)
                    .name(user.getFullName())
                    .email(user.getEmail())
                    .connections(new ArrayList<>())
                    .invitations(new ArrayList<>())
                    .build();
            log.debug("User record before creation '{}'", userEntity);
        }
        try {
            userEntity = userRepository.save(userEntity);
        } catch (DataAccessException ex) {
            log.error("Exception caught when creating/updating user record '{}'", userEntity,
                    ex);
            throw new UserUpdateException(ex);
        }
        return userEntity;
    }

}
