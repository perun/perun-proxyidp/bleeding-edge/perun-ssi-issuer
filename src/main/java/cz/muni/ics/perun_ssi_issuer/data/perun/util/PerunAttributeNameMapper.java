package cz.muni.ics.perun_ssi_issuer.data.perun.util;

import lombok.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
public class PerunAttributeNameMapper {

    // === CLASS FIELDS === //

    private final Map<String, PerunAttributeNameMapping> internalNameMappings = new HashMap<>();
    private final Map<String, PerunAttributeNameMapping> rpcNameMappings = new HashMap<>();

    // === CONSTRUCTORS === //
    public PerunAttributeNameMapper(@NonNull PerunAttributeNameMapperProperties mapperProperties) {
        this.internalNameMappings.putAll(mapperProperties.getMappings());
        initializeRpcMappings();
    }

    // === PUBLIC METHODS === //

    public PerunAttributeNameMapping resolveMappingByInternalName(@NonNull String internalName) {
        if (!StringUtils.hasText(internalName)) {
            throw new IllegalArgumentException("InternalName cannot be empty");
        }
        return internalNameMappings.getOrDefault(internalName, null);
    }

    public PerunAttributeNameMapping resolveMappingByRpcName(@NonNull String rpcName) {
        if (!StringUtils.hasText(rpcName)) {
            throw new IllegalArgumentException("RpcName cannot be empty");
        }
        return rpcNameMappings.getOrDefault(rpcName, null);
    }

    // === PRIVATE METHODS === //

    private void initializeRpcMappings() {
        for (PerunAttributeNameMapping mapping : internalNameMappings.values()) {
            rpcNameMappings.put(mapping.getRpcName(), mapping);
        }
    }

}
